<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Variablecosttahunan extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('Unit_Model');
		$this->loadview('table/variablecosttahunan','');
	}
	public function simpan()
	{
		$datapost = $this->input->post();
		$this->sp_model->exec('variablecosttahunan_simpan',$datapost);
		$unit = $datapost['unit'];
		$tahun = $datapost['tahun'];
		$this->session->set_flashdata('errLoginMsg', 'Data Berhasil Disimpan');
		redirect('variablecosttahunan?unit='.$unit.'&tahun='.$tahun);
	}
}
