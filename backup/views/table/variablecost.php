<?php
$tahun = isset($_GET['tahun'])?$_GET['tahun']:date('Y');
$unit = isset($_GET['unit'])?$_GET['unit']:1;
$namaunit='';

$data = $this->sp_model->exec('variablecost',['tahun'=>$tahun,'unit'=>$unit])['rows'];
$unitall = $this->Unit_Model->getAll()['data'];
?>
<div class="col-md-12">
    <div class="card strpied-tabled-with-hover">
        <div class="card-header ">
            <h4 class="card-title">Tabel Variable Cost</h4>
        </div>
        <form action="" method="get">
          <div class="col-xs-6 col-md-4" style="float:left">
            <label>Unit</label>
            <select class="form-control" name="unit" onchange="this.form.submit()">
              <?php foreach ($unitall as $key => $value) {
                $selected=($unit==$value['doc_id'])?' selected':'';
                $namaunit=($unit==$value['doc_id'])?$value['unit']:$namaunit;
                echo '<option value="'.$value['doc_id'].'"'.$selected.'>'.$value['unit'].'</option>';
              } ?>
            </select>
          </div>
          <div class="col-xs-6 col-md-4" style="float:left">
            <label>Year</label>
            <input type="number" class="form-control" name="tahun" onchange="this.form.submit()" value="<?=$tahun?>" >
          </div>
          <div class="col-xs-6 col-md-4" style="float:right;text-align:right">
            <label style="width:100%">&nbsp;</label>
            <button type="button" onclick="fnExcelReport('tablevariablecost','Variable Cost <?=$namaunit?> (<?=$tahun?>)')" class="btn btn-primary" style="cursor:pointer"> Export Excel</button>
          </div>
        </form>
        <div class="card-body table-full-width table-responsive">
            <table id="tablevariablecost" class="tableedit table table-hover table-striped table-bordered" style="display:block;overflow-x:auto;">
                <thead>
                  <?php foreach ($data[0] as $key => $value) {
                    if ($key=='id_perkiraan') echo ''; else if ($key=='nama_perkiraan') echo '<th></th><th>Keterangan</th>'; else echo '<th>'.lengkapibulan($key).'</th>';
                  } ?>
                </thead>
                <tbody>
                  <?php foreach ($data as $key => $value) {
                    echo '<tr'.(($value->nama_perkiraan=='TOTAL')?' class="total"':'').' id="tr'.$key.'" data-idper="'.$value->id_perkiraan.'">';
                      foreach ($data[$key] as $key2 => $value2) {
                        if ($key2!='id_perkiraan') {
                          if ($key2=='nama_perkiraan') echo '<td><i class="nc-icon nc-bullet-list-67" style="cursor:pointer" onclick="ubahnilai('.$key.')"></i></td><td>'.$value2.'</td>'; else echo '<td style="text-align:right" id="td'.$key.$key2.'" data-nilai="'.$value2.'">'.number_format($value2,2,',','.').'</td>';
                        }
                      }
                    echo '</tr>';
                  } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade modal-primary" id="ubahnilai" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="-webkit-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);">
        <div class="modal-content">
          <form action="<?=base_url()?>variablecost/simpan" method="post">
            <div class="modal-header justify-content-center">
                Ubah Nilai
            </div>
            <div class="modal-body">
              <input type="hidden" name="aksi" value="ubahnilai">
              <input type="hidden" name="tahun" value="<?=$tahun?>">
              <input type="hidden" name="unit" value="<?=$unit?>">
              <input type="hidden" name="id_perkiraan" class="id_perkiraan" value="">
              <table>
                <?php
                foreach (BULAN as $key => $bln) {
                  echo '<tr><td><label>'.BULAN_FULL[$key].'</label></td><td><input id="ubahnilai'.$bln.'" type="number" step="0.00000000000000001" class="form-control" name="'.$bln.'" value="" placeholder="0" /></td></tr>';
                }
                ?>
              </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" style="cursor:pointer">Simpan</button>
            </div>
          </form>
        </div>
    </div>
</div>

<script type="text/javascript">
  function ubahnilai(key=0) {
    $('#ubahnilai').modal();
    $('#ubahnilai .modal-header').html($('#tr'+key+' td:nth-child(2)').html());
    $('#ubahnilai .id_perkiraan').val($('#tr'+key).attr('data-idper'));
    <?php foreach (BULAN as $key => $bln) {
      echo "$('#ubahnilai".$bln."').val($('#td'+key+'".$bln."').attr('data-nilai'));";
    } ?>
  }
</script>
