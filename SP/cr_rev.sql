if exists(select * from sysobjects where [name]='cr_rev')
   drop proc cr_rev
   go
create proc cr_rev
(
@id int,
@tahun int,
@Jan float,
@Feb float,
@Mar float,
@Apr float,
@Mei float,
@Jun float,
@Jul float,
@Agu float,
@Sep float,
@Okt float,
@Nov float,
@Des float
)
as
------------------------Jan-----------------------------------------------------------------
if exists (select id_perkiraan,amount from proyeksi where bln = '1' and tahun = @tahun and id_perkiraan = @id)
   begin
   update proyeksi set amount = @Jan where bln = '1' and tahun = @tahun and id_perkiraan = @id 
   end
   else
   begin
   insert into proyeksi (id_perkiraan,amount,bln,tahun) values (@id,@Jan,'1',@tahun)
   end 
   ------------------------Feb-----------------------------------------------------------------
if exists (select id_perkiraan,amount from proyeksi where bln = '2' and tahun = @tahun and id_perkiraan = @id)
   begin
   update proyeksi set amount = @Feb where bln = '2' and tahun = @tahun and id_perkiraan = @id 
   end
   else
   begin
   insert into proyeksi (id_perkiraan,amount,bln,tahun) values (@id,@Feb,'2',@tahun)
   end 
   ------------------------Mar-----------------------------------------------------------------
if exists (select id_perkiraan,amount from proyeksi where bln = '3' and tahun = @tahun and id_perkiraan = @id)
   begin
   update proyeksi set amount = @Mar where bln = '3' and tahun = @tahun and id_perkiraan = @id 
   end
   else
   begin
   insert into proyeksi (id_perkiraan,amount,bln,tahun) values (@id,@Mar,'3',@tahun)
   end 
   ------------------------Apr-----------------------------------------------------------------
if exists (select id_perkiraan,amount from proyeksi where bln = '4' and tahun = @tahun and id_perkiraan = @id)
   begin
   update proyeksi set amount = @Apr where bln = '4' and tahun = @tahun and id_perkiraan = @id 
   end
   else
   begin
   insert into proyeksi (id_perkiraan,amount,bln,tahun) values (@id,@Apr,'4',@tahun)
   end 
   ------------------------Mei-----------------------------------------------------------------
if exists (select id_perkiraan,amount from proyeksi where bln = '5' and tahun = @tahun and id_perkiraan = @id)
   begin
   update proyeksi set amount = @Mei where bln = '5' and tahun = @tahun and id_perkiraan = @id 
   end
   else
   begin
   insert into proyeksi (id_perkiraan,amount,bln,tahun) values (@id,@Mei,'5',@tahun)
   end 
   ------------------------Jun-----------------------------------------------------------------
if exists (select id_perkiraan,amount from proyeksi where bln = '6' and tahun = @tahun and id_perkiraan = @id)
   begin
   update proyeksi set amount = @Jun where bln = '6' and tahun = @tahun and id_perkiraan = @id 
   end
   else
   begin
   insert into proyeksi (id_perkiraan,amount,bln,tahun) values (@id,@Jun,'6',@tahun)
   end 
   ------------------------Jul-----------------------------------------------------------------
if exists (select id_perkiraan,amount from proyeksi where bln = '7' and tahun = @tahun and id_perkiraan = @id)
   begin
   update proyeksi set amount = @Jul where bln = '7' and tahun = @tahun and id_perkiraan = @id 
   end
   else
   begin
   insert into proyeksi (id_perkiraan,amount,bln,tahun) values (@id,@Jul,'7',@tahun)
   end 
   ------------------------Agu-----------------------------------------------------------------
if exists (select id_perkiraan,amount from proyeksi where bln = '8' and tahun = @tahun and id_perkiraan = @id)
   begin
   update proyeksi set amount = @Agu where bln = '8' and tahun = @tahun and id_perkiraan = @id 
   end
   else
   begin
   insert into proyeksi (id_perkiraan,amount,bln,tahun) values (@id,@Agu,'8',@tahun)
   end 
   ------------------------Sep-----------------------------------------------------------------
if exists (select id_perkiraan,amount from proyeksi where bln = '9' and tahun = @tahun and id_perkiraan = @id)
   begin
   update proyeksi set amount = @Sep where bln = '9' and tahun = @tahun and id_perkiraan = @id 
   end
   else
   begin
   insert into proyeksi (id_perkiraan,amount,bln,tahun) values (@id,@Sep,'9',@tahun)
   end 
   ------------------------Okt-----------------------------------------------------------------
if exists (select id_perkiraan,amount from proyeksi where bln = '10' and tahun = @tahun and id_perkiraan = @id)
   begin
   update proyeksi set amount = @Okt where bln = '10' and tahun = @tahun and id_perkiraan = @id 
   end
   else
   begin
   insert into proyeksi (id_perkiraan,amount,bln,tahun) values (@id,@Okt,'10',@tahun)
   end 
   ------------------------Nov-----------------------------------------------------------------
if exists (select id_perkiraan,amount from proyeksi where bln = '11' and tahun = @tahun and id_perkiraan = @id)
   begin
   update proyeksi set amount = @Nov where bln = '11' and tahun = @tahun and id_perkiraan = @id 
   end
   else
   begin
   insert into proyeksi (id_perkiraan,amount,bln,tahun) values (@id,@Nov,'11',@tahun)
   end 
   ------------------------Des-----------------------------------------------------------------
if exists (select id_perkiraan,amount from proyeksi where bln = '12' and tahun = @tahun and id_perkiraan = @id)
   begin
   update proyeksi set amount = @Des where bln = '12' and tahun = @tahun and id_perkiraan = @id 
   end
   else
   begin
   insert into proyeksi (id_perkiraan,amount,bln,tahun) values (@id,@Des,'12',@tahun)
   end 
go