if exists (select * from sysobjects where [Name]='deviasi')
   drop proc deviasi
   go
create proc deviasi 
(
@tahun varchar(4)
)
as
create table #pnl
(
id_perkiraan int,
nama_perkiraan varchar(200),
Jan float,
Feb float,
Mar float,
Apr float,
Mei float,
Jun float,
Jul float,
Agu float,
Sep float,
Okt float,
Nov float,
Des float
)
insert into #pnl 
select N.doc_id,N.unit as [ket],
			sum(isnull(B.Jan,0))- isnull(O.Jan,0) as [Jan],
			sum(isnull(C.Feb,0))- isnull(O.Feb,0) as [Feb],
			sum(isnull(D.Mar,0))- isnull(O.Mar,0) as [Mar],
			sum(isnull(E.Apr,0))- isnull(O.Apr,0) as [Apr],
			sum(isnull(F.Mei,0))- isnull(O.Mei,0) as [Mei],
			sum(isnull(G.Jun,0))- isnull(O.Jun,0) as [Jun],
			sum(isnull(H.Jul,0))- isnull(O.Jul,0) as [Jul],
			sum(isnull(I.Agu,0))- isnull(O.Agu,0) as [Agu],
			sum(isnull(J.Sep,0))- isnull(O.Sep,0) as [Sep],
			sum(isnull(K.Okt,0))- isnull(O.Okt,0) as [Okt],
			sum(isnull(L.Nov,0))- isnull(O.Nov,0) as [Nov],
			sum(isnull(M.Des,0))- isnull(O.Des,0) as [Des] 
			from m_perkiraan A with(nolock)
			Left Join(select id_perkiraan,amount as [Jan] from proyeksi with(nolock) where bln = 1 and tahun  = @tahun) B on B.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Feb] from proyeksi with(nolock) where bln = 2 and tahun  = @tahun) C on C.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Mar] from proyeksi with(nolock)  where bln = 3 and tahun  = @tahun) D on D.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Apr] from proyeksi with(nolock)  where bln = 4 and tahun  = @tahun) E on E.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Mei] from proyeksi with(nolock)  where bln = 5 and tahun  = @tahun) F on F.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Jun] from proyeksi with(nolock)  where bln = 6 and tahun  = @tahun) G on G.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Jul] from proyeksi with(nolock)  where bln = 7 and tahun  = @tahun) H on H.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Agu] from proyeksi with(nolock)  where bln = 8 and tahun  = @tahun) I on I.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Sep] from proyeksi with(nolock)  where bln = 9 and tahun  = @tahun) J on J.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Okt] from proyeksi with(nolock)  where bln = 10 and tahun  = @tahun) K on K.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Nov] from proyeksi with(nolock)  where bln = 11 and tahun  = @tahun) L on L.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Des] from proyeksi with(nolock)  where bln = 12 and tahun  = @tahun) M on M.id_perkiraan = A.id_perkiraan
			Left Join m_unit N on N.doc_id = A.id_perusahaan
		    Left Join (
						select N.doc_id as [id_unit],'Cost' as [ket],
						sum(isnull(B.Jan,0)) as [Jan],
						sum(isnull(C.Feb,0)) as [Feb],
						sum(isnull(D.Mar,0)) as [Mar],
						sum(isnull(E.Apr,0)) as [Apr],
						sum(isnull(F.Mei,0)) as [Mei],
						sum(isnull(G.Jun,0)) as [Jun],
						sum(isnull(H.Jul,0)) as [Jul],
						sum(isnull(I.Agu,0)) as [Agu],
						sum(isnull(J.Sep,0)) as [Sep],
						sum(isnull(K.Okt,0)) as [Okt],
						sum(isnull(L.Nov,0)) as [Nov],
						sum(isnull(M.Des,0)) as [Des] 
						from m_perkiraan A with(nolock)
						Left Join(select id_perkiraan,amount as [Jan] from proyeksi with(nolock) where bln = 1 and tahun  = @tahun) B on B.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Feb] from proyeksi with(nolock) where bln = 2 and tahun  = @tahun) C on C.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Mar] from proyeksi with(nolock)  where bln = 3 and tahun  = @tahun) D on D.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Apr] from proyeksi with(nolock)  where bln = 4 and tahun  = @tahun) E on E.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Mei] from proyeksi with(nolock)  where bln = 5 and tahun  = @tahun) F on F.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Jun] from proyeksi with(nolock)  where bln = 6 and tahun  = @tahun) G on G.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Jul] from proyeksi with(nolock)  where bln = 7 and tahun  = @tahun) H on H.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Agu] from proyeksi with(nolock)  where bln = 8 and tahun  = @tahun) I on I.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Sep] from proyeksi with(nolock)  where bln = 9 and tahun  = @tahun) J on J.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Okt] from proyeksi with(nolock)  where bln = 10 and tahun  = @tahun) K on K.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Nov] from proyeksi with(nolock)  where bln = 11 and tahun  = @tahun) L on L.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Des] from proyeksi with(nolock)  where bln = 12 and tahun  = @tahun) M on M.id_perkiraan = A.id_perkiraan
						Left Join m_unit N on N.doc_id = A.id_perusahaan
						where A.kode_perkiraan between 2 and 7 and A.status = 1
			            group by N.doc_id,N.unit ) O on O.id_unit = A.id_perusahaan
			where A.kode_perkiraan = 1 and A.status = 1
			group by N.doc_id,N.unit,O.Jan,O.Feb,O.Mar,O.Apr,O.Mei,O.Jun,O.Jul,O.Agu,O.Sep,O.Okt,O.Nov,O.Des

------------------------------------------------------------kredit

create table #debitur
(
nama_perkiraan varchar(200),
Jan float,
Feb float,
Mar float,
Apr float,
Mei float,
Jun float,
Jul float,
Agu float,
Sep float,
Okt float,
Nov float,
Des float
)
insert into #debitur
select 
A.kelompok,
isnull(B.Jan,0) as [Jan],
isnull(C.Feb,0) as [Feb],
isnull(D.Mar,0) as [Mar],
isnull(E.Apr,0) as [Apr],
isnull(F.Mei,0) as [Mei],
isnull(G.Jun,0) as [Jun],
isnull(H.Jul,0) as [Jul],
isnull(I.Agu,0) as [Agu],
isnull(J.Sep,0) as [Sep],
isnull(K.Okt,0) as [Okt],
isnull(L.Nov,0) as [Nov],
isnull(M.Des,0) as [Des] 
from kelompok A
Left Join (
			select sum(amount) as [Jan] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-01-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-01-01')+1,0))))
			and remark = 'pencairan'
		  )B on B.Jan = B.Jan
Left Join (
			select sum(amount) as [Feb] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-02-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-02-01')+1,0))))
			and remark = 'pencairan'
		  )C on C.Feb = C.Feb
Left Join (
			select sum(amount) as [Mar] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-03-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-03-01')+1,0))))
			and remark = 'pencairan'
		  )D on D.Mar = D.Mar
Left Join (
			select sum(amount) as [Apr] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-04-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-04-01')+1,0))))
			and remark = 'pencairan'
		  )E on E.Apr = E.Apr
Left Join (
			select sum(amount) as [Mei] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-05-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-05-01')+1,0))))
			and remark = 'pencairan'
		  )F on F.Mei = F.Mei
Left Join (
			select sum(amount) as [Jun] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-06-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-06-01')+1,0))))
			and remark = 'pencairan'
		  )G on G.Jun = G.Jun
Left Join (
			select sum(amount) as [Jul] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-07-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-07-01')+1,0))))
			and remark = 'pencairan'
		  )H on H.Jul = H.Jul
Left Join (
			select sum(amount) as [Agu] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-08-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-08-01')+1,0))))
			and remark = 'pencairan'
		  )I on I.Agu = I.Agu
Left Join (
			select sum(amount) as [Sep] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-09-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-09-01')+1,0))))
			and remark = 'pencairan'
		  )J on J.Sep = J.Sep
Left Join (
			select sum(amount) as [Okt] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-10-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-10-01')+1,0))))
			and remark = 'pencairan'
		  )K on K.Okt = K.Okt
Left Join (
			select sum(amount) as [Nov] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-11-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-11-01')+1,0))))
			and remark = 'pencairan'
		  )L on L.Nov = L.Nov
Left Join (
			select sum(amount) as [Des] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-12-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-12-01')+1,0))))
			and remark = 'pencairan'
		  )M on M.Des = M.Des
where A.doc_id = 8
----------------------------------------------------------------pihak 3
create table #bayar
(
nama_perkiraan varchar(200),
Jan float,
Feb float,
Mar float,
Apr float,
Mei float,
Jun float,
Jul float,
Agu float,
Sep float,
Okt float,
Nov float,
Des float
)
insert into #bayar
select 
'PEMBAYARAN HUTANG' as [kelompok],
isnull(B.Jan,0) as [Jan],
isnull(C.Feb,0) as [Feb],
isnull(D.Mar,0) as [Mar],
isnull(E.Apr,0) as [Apr],
isnull(F.Mei,0) as [Mei],
isnull(G.Jun,0) as [Jun],
isnull(H.Jul,0) as [Jul],
isnull(I.Agu,0) as [Agu],
isnull(J.Sep,0) as [Sep],
isnull(K.Okt,0) as [Okt],
isnull(L.Nov,0) as [Nov],
isnull(M.Des,0) as [Des] 
from kelompok A
Left Join (
			select sum(amount) as [Jan] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-01-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-01-01')+1,0))))
			and remark = 'pembayaran'
		  )B on B.Jan = B.Jan
Left Join (
			select sum(amount) as [Feb] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-02-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-02-01')+1,0))))
			and remark = 'pembayaran'
		  )C on C.Feb = C.Feb
Left Join (
			select sum(amount) as [Mar] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-03-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-03-01')+1,0))))
			and remark = 'pembayaran'
		  )D on D.Mar = D.Mar
Left Join (
			select sum(amount) as [Apr] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-04-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-04-01')+1,0))))
			and remark = 'pembayaran'
		  )E on E.Apr = E.Apr
Left Join (
			select sum(amount) as [Mei] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-05-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-05-01')+1,0))))
			and remark = 'pembayaran'
		  )F on F.Mei = F.Mei
Left Join (
			select sum(amount) as [Jun] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-06-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-06-01')+1,0))))
			and remark = 'pembayaran'
		  )G on G.Jun = G.Jun
Left Join (
			select sum(amount) as [Jul] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-07-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-07-01')+1,0))))
			and remark = 'pembayaran'
		  )H on H.Jul = H.Jul
Left Join (
			select sum(amount) as [Agu] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-08-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-08-01')+1,0))))
			and remark = 'pembayaran'
		  )I on I.Agu = I.Agu
Left Join (
			select sum(amount) as [Sep] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-09-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-09-01')+1,0))))
			and remark = 'pembayaran'
		  )J on J.Sep = J.Sep
Left Join (
			select sum(amount) as [Okt] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-10-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-10-01')+1,0))))
			and remark = 'pembayaran'
		  )K on K.Okt = K.Okt
Left Join (
			select sum(amount) as [Nov] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-11-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-11-01')+1,0))))
			and remark = 'pembayaran'
		  )L on L.Nov = L.Nov
Left Join (
			select sum(amount) as [Des] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-12-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-12-01')+1,0))))
			and remark = 'pembayaran'
		  )M on M.Des = M.Des
where A.doc_id = 9
----------------------------------------------------kalkulasi
select 'Deviasi' as [ket],
(sum(A.Jan)+B.Jan+C.Jan) as [Jan],
(sum(A.Feb)+B.Feb+C.Feb) as [Feb],
(sum(A.Mar)+B.Mar+C.Mar) as [Mar],
(sum(A.Apr)+B.Apr+C.Apr) as [Apr],
(sum(A.Mei)+B.Mei+C.Mei) as [Mei],
(sum(A.Jun)+B.Jun+C.Jun) as [Jun],
(sum(A.Jul)+B.Jul+C.Jul) as [Jul],
(sum(A.Agu)+B.Agu+C.Agu) as [Agu],
(sum(A.Sep)+B.Sep+C.Sep) as [Sep],
(sum(A.Okt)+B.Okt+C.Okt) as [Okt],
(sum(A.Nov)+B.Nov+C.Nov) as [Nov],
(sum(A.Des)+B.Des+C.Des) as [Des] 
from #pnl A
Left Join (
			select 'Debitur' as [ket],
			sum(Jan) as [Jan],
			sum(Feb) as [Feb],
			sum(Mar) as [Mar],
			sum(Apr) as [Apr],
			sum(Mei) as [Mei],
			sum(Jun) as [Jun],
			sum(Jul) as [Jul],
			sum(Agu) as [Agu],
			sum(Sep) as [Sep],
			sum(Okt) as [Okt],
			sum(Nov) as [Nov],
			sum(Des) as [Des] 
			from #debitur
			) B on B.ket = B.ket
Left Join (
			select 'Pembayaran' as [ket],
			sum(Jan) as [Jan],
			sum(Feb) as [Feb],
			sum(Mar) as [Mar],
			sum(Apr) as [Apr],
			sum(Mei) as [Mei],
			sum(Jun) as [Jun],
			sum(Jul) as [Jul],
			sum(Agu) as [Agu],
			sum(Sep) as [Sep],
			sum(Okt) as [Okt],
			sum(Nov) as [Nov],
			sum(Des) as [Des] 
			from #bayar
			) C on C.ket = C.ket
Group by B.Jan,B.Feb,B.Mar,B.Apr,B.Mei,B.Jun,B.Jul,B.Agu,B.Sep,B.Okt,B.Nov,B.Des,
C.Jan,C.Feb,C.Mar,C.Apr,C.Mei,C.Jun,C.Jul,C.Agu,C.Sep,C.Okt,C.Nov,C.Des

go
