If exists(select * from sysobjects where [name]='hp3')
   drop proc hp3
   go
Create Proc hp3
(
@tahun varchar(4)
)
as
select 
'PEMBAYARAN HUTANG' as [kelompok],
isnull(B.Jan,0) as [Jan],
isnull(C.Feb,0) as [Feb],
isnull(D.Mar,0) as [Mar],
isnull(E.Apr,0) as [Apr],
isnull(F.Mei,0) as [Mei],
isnull(G.Jun,0) as [Jun],
isnull(H.Jul,0) as [Jul],
isnull(I.Agu,0) as [Agu],
isnull(J.Sep,0) as [Sep],
isnull(K.Okt,0) as [Okt],
isnull(L.Nov,0) as [Nov],
isnull(M.Des,0) as [Des] 
from kelompok A
Left Join (
			select sum(amount) as [Jan] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-01-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-01-01')+1,0))))
			and remark = 'pembayaran'
		  )B on B.Jan = B.Jan
Left Join (
			select sum(amount) as [Feb] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-02-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-02-01')+1,0))))
			and remark = 'pembayaran'
		  )C on C.Feb = C.Feb
Left Join (
			select sum(amount) as [Mar] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-03-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-03-01')+1,0))))
			and remark = 'pembayaran'
		  )D on D.Mar = D.Mar
Left Join (
			select sum(amount) as [Apr] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-04-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-04-01')+1,0))))
			and remark = 'pembayaran'
		  )E on E.Apr = E.Apr
Left Join (
			select sum(amount) as [Mei] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-05-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-05-01')+1,0))))
			and remark = 'pembayaran'
		  )F on F.Mei = F.Mei
Left Join (
			select sum(amount) as [Jun] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-06-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-06-01')+1,0))))
			and remark = 'pembayaran'
		  )G on G.Jun = G.Jun
Left Join (
			select sum(amount) as [Jul] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-07-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-07-01')+1,0))))
			and remark = 'pembayaran'
		  )H on H.Jul = H.Jul
Left Join (
			select sum(amount) as [Agu] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-08-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-08-01')+1,0))))
			and remark = 'pembayaran'
		  )I on I.Agu = I.Agu
Left Join (
			select sum(amount) as [Sep] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-09-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-09-01')+1,0))))
			and remark = 'pembayaran'
		  )J on J.Sep = J.Sep
Left Join (
			select sum(amount) as [Okt] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-10-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-10-01')+1,0))))
			and remark = 'pembayaran'
		  )K on K.Okt = K.Okt
Left Join (
			select sum(amount) as [Nov] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-11-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-11-01')+1,0))))
			and remark = 'pembayaran'
		  )L on L.Nov = L.Nov
Left Join (
			select sum(amount) as [Des] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-12-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-12-01')+1,0))))
			and remark = 'pembayaran'
		  )M on M.Des = M.Des
where A.doc_id = 9
go