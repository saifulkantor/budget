USE [kb]
GO
/****** Object:  StoredProcedure [dbo].[cu_user]    Script Date: 12/12/2019 11:21:09 ******/
CREATE proc [dbo].[cu_user_unit]
(
@id_user bigint=0,
@id_unit bigint=0
)
as

if exists (select id_user,id_unit from m_user_unit where id_user = @id_user and id_unit = @id_unit)
   begin
   update m_user_unit set id_unit=@id_unit where id_user = @id_user
   end
   else
   begin
   insert into m_user_unit (id_user,id_unit) values (@id_user,@id_unit)
   end 