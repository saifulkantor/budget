if exists(select * from sysobjects where [name]='variablecosttahunan')
   drop proc variablecosttahunan
   go
Create Proc variablecosttahunan
(
@tahun int,
@unit int
)
as
create table #varcost 
(
id_perkiraan int,
nama_perkiraan varchar(200),
Jan float,
Feb float,
Mar float,
Apr float,
Mei float,
Jun float,
Jul float,
Agu float,
Sep float,
Okt float,
Nov float,
Des float
)
insert into #varcost
select 
A.id_perkiraan,
A.nama_perkiraan,
isnull(B.Jan,0) as [Jan],
isnull(C.Feb,0) as [Feb],
isnull(D.Mar,0) as [Mar],
isnull(E.Apr,0) as [Apr],
isnull(F.Mei,0) as [Mei],
isnull(G.Jun,0) as [Jun],
isnull(H.Jul,0) as [Jul],
isnull(I.Agu,0) as [Agu],
isnull(J.Sep,0) as [Sep],
isnull(K.Okt,0) as [Okt],
isnull(L.Nov,0) as [Nov],
isnull(M.Des,0) as [Des] 
from m_perkiraan A with(nolock)
Left Join(select id_perkiraan,amount as [Jan] from proyeksi with(nolock) where bln = 1 and tahun  = @tahun) B on B.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Feb] from proyeksi with(nolock) where bln = 2 and tahun  = @tahun) C on C.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Mar] from proyeksi with(nolock)  where bln = 3 and tahun  = @tahun) D on D.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Apr] from proyeksi with(nolock)  where bln = 4 and tahun  = @tahun) E on E.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Mei] from proyeksi with(nolock)  where bln = 5 and tahun  = @tahun) F on F.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Jun] from proyeksi with(nolock)  where bln = 6 and tahun  = @tahun) G on G.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Jul] from proyeksi with(nolock)  where bln = 7 and tahun  = @tahun) H on H.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Agu] from proyeksi with(nolock)  where bln = 8 and tahun  = @tahun) I on I.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Sep] from proyeksi with(nolock)  where bln = 9 and tahun  = @tahun) J on J.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Okt] from proyeksi with(nolock)  where bln = 10 and tahun  = @tahun) K on K.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Nov] from proyeksi with(nolock)  where bln = 11 and tahun  = @tahun) L on L.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Des] from proyeksi with(nolock)  where bln = 12 and tahun  = @tahun) M on M.id_perkiraan = A.id_perkiraan
where A.kode_perkiraan = 4 and A.id_perusahaan = @unit and A.status = 1
-------------------------------------------
select
id_perkiraan,
nama_perkiraan,
Jan,Feb,Mar,Apr,Mei,Jun,Jul,Agu,Sep,Okt,Nov,Des
from #varcost
Union All
select
0 as [id_perkiraan],
'TOTAL' as [nama_perkiraan],
 isnull(sum(Jan),0) as [Jan]
,isnull(sum(Feb),0) as [Feb]
,isnull(sum(Mar),0) as [Mar]
,isnull(sum(Apr),0) as [Apr]
,isnull(sum(Mei),0) as [Mei]
,isnull(sum(Jun),0) as [Jun]
,isnull(sum(Jul),0) as [Jul]
,isnull(sum(Agu),0) as [Agu]
,isnull(sum(Sep),0) as [Sep]
,isnull(sum(Okt),0) as [Okt]
,isnull(sum(Nov),0) as [Nov]
,isnull(sum(Des),0) as [Des]
from #varcost
go






