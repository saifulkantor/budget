
if exists(select * from sysobjects where [name]='sum_utang')
   drop proc sum_utang
   go
create proc sum_utang
(
@tahun int
)
as
select A.doc_id,A.unit,isnull(B.[ket],'-') as [ket],
isnull(B.Jan,0) as [Jan],
isnull(B.Feb,0) as [Feb],
isnull(B.Mar,0) as [Mar],
isnull(B.Apr,0) as [Apr],
isnull(B.Mei,0) as [Mei],
isnull(B.Jun,0) as [Jun],
isnull(B.Jul,0) as [Jul],
isnull(B.Agu,0) as [Agu],
isnull(B.Sep,0) as [Sep],
isnull(B.Okt,0) as [Okt],
isnull(B.Nov,0) as [Nov],
isnull(B.Des,0) as [Des]
from m_unit A
Left Join (
			select N.doc_id,N.unit as [ket],
			sum(isnull(B.Jan,0))- isnull(O.Jan,0) as [Jan],
			sum(isnull(C.Feb,0))- isnull(O.Feb,0) as [Feb],
			sum(isnull(D.Mar,0))- isnull(O.Mar,0) as [Mar],
			sum(isnull(E.Apr,0))- isnull(O.Apr,0) as [Apr],
			sum(isnull(F.Mei,0))- isnull(O.Mei,0) as [Mei],
			sum(isnull(G.Jun,0))- isnull(O.Jun,0) as [Jun],
			sum(isnull(H.Jul,0))- isnull(O.Jul,0) as [Jul],
			sum(isnull(I.Agu,0))- isnull(O.Agu,0) as [Agu],
			sum(isnull(J.Sep,0))- isnull(O.Sep,0) as [Sep],
			sum(isnull(K.Okt,0))- isnull(O.Okt,0) as [Okt],
			sum(isnull(L.Nov,0))- isnull(O.Nov,0) as [Nov],
			sum(isnull(M.Des,0))- isnull(O.Des,0) as [Des] 
			from m_perkiraan A with(nolock)
			Left Join(select id_perkiraan,amount as [Jan] from proyeksi with(nolock) where bln = 1 and tahun  = @tahun) B on B.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Feb] from proyeksi with(nolock) where bln = 2 and tahun  = @tahun) C on C.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Mar] from proyeksi with(nolock)  where bln = 3 and tahun  = @tahun) D on D.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Apr] from proyeksi with(nolock)  where bln = 4 and tahun  = @tahun) E on E.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Mei] from proyeksi with(nolock)  where bln = 5 and tahun  = @tahun) F on F.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Jun] from proyeksi with(nolock)  where bln = 6 and tahun  = @tahun) G on G.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Jul] from proyeksi with(nolock)  where bln = 7 and tahun  = @tahun) H on H.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Agu] from proyeksi with(nolock)  where bln = 8 and tahun  = @tahun) I on I.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Sep] from proyeksi with(nolock)  where bln = 9 and tahun  = @tahun) J on J.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Okt] from proyeksi with(nolock)  where bln = 10 and tahun  = @tahun) K on K.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Nov] from proyeksi with(nolock)  where bln = 11 and tahun  = @tahun) L on L.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Des] from proyeksi with(nolock)  where bln = 12 and tahun  = @tahun) M on M.id_perkiraan = A.id_perkiraan
			Left Join m_unit N on N.doc_id = A.id_perusahaan
		    Left Join (
						select N.doc_id as [id_unit],'Cost' as [ket],
						sum(isnull(B.Jan,0)) as [Jan],
						sum(isnull(C.Feb,0)) as [Feb],
						sum(isnull(D.Mar,0)) as [Mar],
						sum(isnull(E.Apr,0)) as [Apr],
						sum(isnull(F.Mei,0)) as [Mei],
						sum(isnull(G.Jun,0)) as [Jun],
						sum(isnull(H.Jul,0)) as [Jul],
						sum(isnull(I.Agu,0)) as [Agu],
						sum(isnull(J.Sep,0)) as [Sep],
						sum(isnull(K.Okt,0)) as [Okt],
						sum(isnull(L.Nov,0)) as [Nov],
						sum(isnull(M.Des,0)) as [Des] 
						from m_perkiraan A with(nolock)
						Left Join(select id_perkiraan,amount as [Jan] from proyeksi with(nolock) where bln = 1 and tahun  = @tahun) B on B.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Feb] from proyeksi with(nolock) where bln = 2 and tahun  = @tahun) C on C.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Mar] from proyeksi with(nolock)  where bln = 3 and tahun  = @tahun) D on D.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Apr] from proyeksi with(nolock)  where bln = 4 and tahun  = @tahun) E on E.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Mei] from proyeksi with(nolock)  where bln = 5 and tahun  = @tahun) F on F.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Jun] from proyeksi with(nolock)  where bln = 6 and tahun  = @tahun) G on G.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Jul] from proyeksi with(nolock)  where bln = 7 and tahun  = @tahun) H on H.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Agu] from proyeksi with(nolock)  where bln = 8 and tahun  = @tahun) I on I.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Sep] from proyeksi with(nolock)  where bln = 9 and tahun  = @tahun) J on J.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Okt] from proyeksi with(nolock)  where bln = 10 and tahun  = @tahun) K on K.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Nov] from proyeksi with(nolock)  where bln = 11 and tahun  = @tahun) L on L.id_perkiraan = A.id_perkiraan
						Left Join(select id_perkiraan,amount as [Des] from proyeksi with(nolock)  where bln = 12 and tahun  = @tahun) M on M.id_perkiraan = A.id_perkiraan
						Left Join m_unit N on N.doc_id = A.id_perusahaan
						where A.kode_perkiraan between 2 and 7 
			            group by N.doc_id,N.unit ) O on O.id_unit = A.id_perusahaan
			where A.kode_perkiraan = 1 
			group by N.doc_id,N.unit,O.Jan,O.Feb,O.Mar,O.Apr,O.Mei,O.Jun,O.Jul,O.Agu,O.Sep,O.Okt,O.Nov,O.Des
			Union All-------------------------------------------------------------------------------------------------
			select N.doc_id,'Rev' as [ket],
			sum(isnull(B.Jan,0)) as [Jan],
			sum(isnull(C.Feb,0)) as [Feb],
			sum(isnull(D.Mar,0)) as [Mar],
			sum(isnull(E.Apr,0)) as [Apr],
			sum(isnull(F.Mei,0)) as [Mei],
			sum(isnull(G.Jun,0)) as [Jun],
			sum(isnull(H.Jul,0)) as [Jul],
			sum(isnull(I.Agu,0)) as [Agu],
			sum(isnull(J.Sep,0)) as [Sep],
			sum(isnull(K.Okt,0)) as [Okt],
			sum(isnull(L.Nov,0)) as [Nov],
			sum(isnull(M.Des,0)) as [Des] 
			from m_perkiraan A with(nolock)
			Left Join(select id_perkiraan,amount as [Jan] from proyeksi with(nolock) where bln = 1 and tahun  = @tahun) B on B.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Feb] from proyeksi with(nolock) where bln = 2 and tahun  = @tahun) C on C.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Mar] from proyeksi with(nolock)  where bln = 3 and tahun  = @tahun) D on D.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Apr] from proyeksi with(nolock)  where bln = 4 and tahun  = @tahun) E on E.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Mei] from proyeksi with(nolock)  where bln = 5 and tahun  = @tahun) F on F.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Jun] from proyeksi with(nolock)  where bln = 6 and tahun  = @tahun) G on G.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Jul] from proyeksi with(nolock)  where bln = 7 and tahun  = @tahun) H on H.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Agu] from proyeksi with(nolock)  where bln = 8 and tahun  = @tahun) I on I.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Sep] from proyeksi with(nolock)  where bln = 9 and tahun  = @tahun) J on J.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Okt] from proyeksi with(nolock)  where bln = 10 and tahun  = @tahun) K on K.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Nov] from proyeksi with(nolock)  where bln = 11 and tahun  = @tahun) L on L.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Des] from proyeksi with(nolock)  where bln = 12 and tahun  = @tahun) M on M.id_perkiraan = A.id_perkiraan
			Left Join m_unit N on N.doc_id = A.id_perusahaan
			where A.kode_perkiraan = 1 
			group by N.doc_id,N.unit
			Union All------------------------------------------------------
			select N.doc_id,'Cost' as [ket],
			sum(isnull(B.Jan,0)) as [Jan],
			sum(isnull(C.Feb,0)) as [Feb],
			sum(isnull(D.Mar,0)) as [Mar],
			sum(isnull(E.Apr,0)) as [Apr],
			sum(isnull(F.Mei,0)) as [Mei],
			sum(isnull(G.Jun,0)) as [Jun],
			sum(isnull(H.Jul,0)) as [Jul],
			sum(isnull(I.Agu,0)) as [Agu],
			sum(isnull(J.Sep,0)) as [Sep],
			sum(isnull(K.Okt,0)) as [Okt],
			sum(isnull(L.Nov,0)) as [Nov],
			sum(isnull(M.Des,0)) as [Des] 
			from m_perkiraan A with(nolock)
			Left Join(select id_perkiraan,amount as [Jan] from proyeksi with(nolock) where bln = 1 and tahun  = @tahun) B on B.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Feb] from proyeksi with(nolock) where bln = 2 and tahun  = @tahun) C on C.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Mar] from proyeksi with(nolock)  where bln = 3 and tahun  = @tahun) D on D.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Apr] from proyeksi with(nolock)  where bln = 4 and tahun  = @tahun) E on E.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Mei] from proyeksi with(nolock)  where bln = 5 and tahun  = @tahun) F on F.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Jun] from proyeksi with(nolock)  where bln = 6 and tahun  = @tahun) G on G.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Jul] from proyeksi with(nolock)  where bln = 7 and tahun  = @tahun) H on H.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Agu] from proyeksi with(nolock)  where bln = 8 and tahun  = @tahun) I on I.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Sep] from proyeksi with(nolock)  where bln = 9 and tahun  = @tahun) J on J.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Okt] from proyeksi with(nolock)  where bln = 10 and tahun  = @tahun) K on K.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Nov] from proyeksi with(nolock)  where bln = 11 and tahun  = @tahun) L on L.id_perkiraan = A.id_perkiraan
			Left Join(select id_perkiraan,amount as [Des] from proyeksi with(nolock)  where bln = 12 and tahun  = @tahun) M on M.id_perkiraan = A.id_perkiraan
			Left Join m_unit N on N.doc_id = A.id_perusahaan
			where A.kode_perkiraan between 2 and 7 
			group by N.doc_id,N.unit
) B on B.doc_id = A.doc_id
go
