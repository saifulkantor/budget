if exists(select * from sysobjects where [name]='laporan')
   drop proc laporan
   go
Create Proc laporan
(
@tahun int,
@unit int
)
as
SELECT Kel.doc_id,
A.nama_perkiraan,
Kel.kelompok,
SUM(isnull(B.Jan,0)) as [Jan],
SUM(isnull(C.Feb,0)) as [Feb],
SUM(isnull(D.Mar,0)) as [Mar],
SUM(isnull(E.Apr,0)) as [Apr],
SUM(isnull(F.Mei,0)) as [Mei],
SUM(isnull(G.Jun,0)) as [Jun],
SUM(isnull(H.Jul,0)) as [Jul],
SUM(isnull(I.Agu,0)) as [Agu],
SUM(isnull(J.Sep,0)) as [Sep],
SUM(isnull(K.Okt,0)) as [Okt],
SUM(isnull(L.Nov,0)) as [Nov],
SUM(isnull(M.Des,0)) as [Des]
from m_perkiraan A with(nolock)
Left Join(select id_perkiraan,amount as [Jan] from proyeksi with(nolock) where bln = 1 and tahun  = 2019) B on B.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Feb] from proyeksi with(nolock) where bln = 2 and tahun  = 2019) C on C.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Mar] from proyeksi with(nolock)  where bln = 3 and tahun  = 2019) D on D.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Apr] from proyeksi with(nolock)  where bln = 4 and tahun  = 2019) E on E.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Mei] from proyeksi with(nolock)  where bln = 5 and tahun  = 2019) F on F.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Jun] from proyeksi with(nolock)  where bln = 6 and tahun  = 2019) G on G.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Jul] from proyeksi with(nolock)  where bln = 7 and tahun  = 2019) H on H.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Agu] from proyeksi with(nolock)  where bln = 8 and tahun  = 2019) I on I.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Sep] from proyeksi with(nolock)  where bln = 9 and tahun  = 2019) J on J.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Okt] from proyeksi with(nolock)  where bln = 10 and tahun  = 2019) K on K.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Nov] from proyeksi with(nolock)  where bln = 11 and tahun  = 2019) L on L.id_perkiraan = A.id_perkiraan
Left Join(select id_perkiraan,amount as [Des] from proyeksi with(nolock)  where bln = 12 and tahun  = 2019) M on M.id_perkiraan = A.id_perkiraan
Left Join kelompok Kel on Kel.doc_id = A.kode_perkiraan
where A.id_perusahaan = 2 and A.status = 1 group by Kel.doc_id, Kel.kelompok, A.nama_perkiraan order by CASE Kel.doc_id
		WHEN 1009 THEN 0
        ELSE Kel.doc_id END
		ASC
go
