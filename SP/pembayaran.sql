if exists (select * from sysobjects where [Name]='pembayaran')
   drop proc pembayaran
   go
create proc pembayaran
(
@tahun varchar(4)
)
as
create table #cair
(
bank varchar(200),
id_bank int,
unit varchar(200),
id_unit int,
Jan float,
Feb float,
Mar float,
Apr float,
Mei float,
Jun float,
Jul float,
Agu float,
Sep float,
Okt float,
Nov float,
Des float,
YTD float
)
insert into #cair
select distinct B.bank,B.doc_id as [id_bank]
,C.unit,C.doc_id as [id_unit]
,isnull(D.Jan,0) AS [Jan]
,isnull(E.Feb,0) AS [Feb]
,isnull(F.Mar,0) AS [Mar]
,isnull(G.Apr,0) AS [Apr]
,isnull(H.Mei,0) AS [Mei]
,isnull(I.Jun,0) AS [Jun]
,isnull(J.Jul,0) AS [Jul]
,isnull(K.Agu,0) AS [Agu]
,isnull(L.Sep,0) AS [Sep]
,isnull(M.Okt,0) AS [Okt]
,isnull(N.Nov,0) AS [Nov]
,isnull(O.Des,0) AS [Des]
----------------------------------
,(isnull(D.Jan,0)
+isnull(E.Feb,0)
+isnull(F.Mar,0)
+isnull(G.Apr,0)
+isnull(H.Mei,0)
+isnull(I.Jun,0)
+isnull(J.Jul,0)
+isnull(K.Agu,0)
+isnull(L.Sep,0)
+isnull(M.Okt,0)
+isnull(N.Nov,0)
+isnull(O.Des,0)) as [ytd]
from bank2 A
Left Join bank B on B.doc_id = A.id_bank
Left Join m_unit C on C.doc_id = A.id_unit
Left Join (
            select id_bank,id_unit,isnull(sum(amount),0) as [Jan] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-01-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-01-01')+1,0))))
			and remark = 'pembayaran' group by id_bank,id_unit
          )D on D.id_bank = A.id_bank and D.id_unit = A.id_unit
Left Join (
            select id_bank,id_unit,isnull(sum(amount),0) as [Feb] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-02-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-02-01')+1,0))))
			and remark = 'pembayaran' group by id_bank,id_unit
          )E on E.id_bank = A.id_bank and E.id_unit = A.id_unit
Left Join (
            select id_bank,id_unit,isnull(sum(amount),0) as [Mar] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-03-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-03-01')+1,0))))
			and remark = 'pembayaran' group by id_bank,id_unit
          )F on F.id_bank = A.id_bank and F.id_unit = A.id_unit
Left Join (
            select id_bank,id_unit,isnull(sum(amount),0) as [Apr] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-04-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-04-01')+1,0))))
			and remark = 'pembayaran' group by id_bank,id_unit
          )G on G.id_bank = A.id_bank and G.id_unit = A.id_unit
Left Join (
            select id_bank,id_unit,isnull(sum(amount),0) as [Mei] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-05-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-05-01')+1,0))))
			and remark = 'pembayaran' group by id_bank,id_unit
          )H on H.id_bank = A.id_bank and H.id_unit = A.id_unit
Left Join (
            select id_bank,id_unit,isnull(sum(amount),0) as [Jun] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-06-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-06-01')+1,0))))
			and remark = 'pembayaran' group by id_bank,id_unit
          )I ON I.id_bank = A.id_bank and I.id_unit = A.id_unit
Left Join (
            select id_bank,id_unit,isnull(sum(amount),0) as [Jul] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-07-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-07-01')+1,0))))
			and remark = 'pembayaran' group by id_bank,id_unit
          )J on J.id_bank = A.id_bank and J.id_unit = A.id_unit
Left Join (
            select id_bank,id_unit,isnull(sum(amount),0) as [Agu] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-08-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-08-01')+1,0))))
			and remark = 'pembayaran' group by id_bank,id_unit
          )K on K.id_bank = A.id_bank and K.id_unit = A.id_unit
Left Join (
            select id_bank,id_unit,isnull(sum(amount),0) as [Sep] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-09-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-09-01')+1,0))))
			and remark = 'pembayaran' group by id_bank,id_unit
          )L on L.id_bank = A.id_bank and L.id_unit = A.id_unit
Left Join (
            select id_bank,id_unit,isnull(sum(amount),0) as [Okt] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-10-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-10-01')+1,0))))
			and remark = 'pembayaran' group by id_bank,id_unit
          )M on M.id_bank = A.id_bank and M.id_unit = A.id_unit
Left Join (
            select id_bank,id_unit,isnull(sum(amount),0) as [Nov] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-11-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-11-01')+1,0))))
			and remark = 'pembayaran' group by id_bank,id_unit
          )N on N.id_bank = A.id_bank and N.id_unit = A.id_unit
Left Join (
            select id_bank,id_unit,isnull(sum(amount),0) as [Des] 
			from bank2
			where tgl between
			(select convert(date,DATEADD(s,1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-12-01'),0))))
			and 
			(select convert(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@tahun+'-12-01')+1,0))))
			and remark = 'pembayaran' group by id_bank,id_unit
          )O on O.id_bank = A.id_bank and O.id_unit = A.id_unit
where A.remark = 'pembayaran' and year(A.tgl) = @tahun
------------------------------------------------------------------------------
select 
bank,
id_bank,
unit,
id_unit,
Jan,
Feb,
Mar,
Apr,
Mei,
Jun,
Jul,
Agu,
Sep,
Okt,
Nov,
Des,
YTD  
from #cair
Union All
select 
null as [bank],
null as [id_bank],
'TOTAL' as [unit],
null as [id_unit],
sum(Jan),
sum(Feb),
sum(Mar),
sum(Apr),
sum(Mei),
sum(Jun),
sum(Jul),
sum(Agu),
sum(Sep),
sum(Okt),
sum(Nov),
sum(Des),
sum(YTD)  
from #cair
go