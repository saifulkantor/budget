<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array('database', 'session');

$autoload['drivers'] = array();

$autoload['helper'] = array('url', 'function');
// $autoload['helper'] = array('url', 'function', 'database');

$autoload['config'] = array();

$autoload['language'] = array();

$autoload['model'] = array('sp_model','sp2_model','Kelompok_Model','Perkiraan_Model');
