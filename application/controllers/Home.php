<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('Unit_Model');
		$this->load->model('Kelompok_Model');

		$this->loadview('home',['table'=>'']);
	}

	public function backup()
	{
		$query = $this->db->query("BACKUP DATABASE kb TO DISK = 'C:\\xampp\\htdocs\\dbase\\kb.bak' WITH COMPRESSION");
		redirect('');
	}

	public function account()
	{
		$id_user = $this->input->post('id_user');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$newpassword = $this->input->post('newpassword');
		$newpassword2 = $this->input->post('newpassword2');

		if ($id_user!='') {
			$this->load->model('User_Model');
			$user = $this->User_Model->get($id_user);
			if ($user){
				if (password_verify($password,$user->password)) {
					if ($username!='') {
						$this->User_Model->update(['username'=>$username],['id_user'=>$id_user]);
						$this->session->set_userdata('username',$username);
					} elseif ($nama!=''&&$email!='') {
						$this->User_Model->update(['nama'=>$nama,'email'=>$email,],['id_user'=>$id_user]);
						$this->session->set_userdata('nama',$nama);
						$this->session->set_userdata('email',$email);
					} elseif ($newpassword!=''&&$newpassword2!='') {
						if ($newpassword==$newpassword2) {
							$this->User_Model->update(['password'=>password_hash($newpassword, PASSWORD_BCRYPT)],['id_user'=>$id_user]);
						} else {
							$this->session->set_flashdata('errLoginMsg', 'Password Baru dan Ulangi Password Baru Tidak Sama');
						}
					}
				} else {
					$this->session->set_flashdata('errLoginMsg', 'Password Salah');
				}
			} else {
				$this->session->set_flashdata('errLoginMsg', 'User Tidak Ditemukan');
			}
		}


		$this->loadview('account',['table'=>'']);
	}

	public function user()
	{
		$session = $this->session->userdata();
		if ($session['hakakses']=='admin') {
			$this->load->model('User_Model');
			$this->load->model('Unit_Model');
			$datapost = $this->input->post();
			$proses = true;
			if (isset($datapost['id_user'])) {
				$id_user = $datapost['id_user'];
				$newpassword = $datapost['newpassword'];
				$newpassword2 = $datapost['newpassword2'];
				$user = $this->User_Model->get($id_user);
				$datapost['auth_key']='';
				if ($user){
					$datapost['password']=$user->password;
					$datapost['auth_key']=$user->auth_key;
				}
				if ($newpassword!='') {
					$datapost['password']=password_hash($newpassword, PASSWORD_BCRYPT);
				}
				if ($newpassword!=''&&$newpassword2!='') {
					if ($newpassword==$newpassword2) {
						$datapost['password']=password_hash($newpassword, PASSWORD_BCRYPT);
						$this->session->set_flashdata('errLoginMsg', 'Data Berhasil Disimpan');
					} else {
						$proses=false;
						$this->session->set_flashdata('errLoginMsg', 'Password Baru dan Ulangi Password Baru Tidak Sama');
					}
				} else {
					$this->session->set_flashdata('errLoginMsg', 'Data Berhasil Disimpan');
				}
				if ($proses) {
					$datauser = $this->sp_model->exec('user',$datapost);
					if ($datapost['hakakses']=='user') {
						$query = $this->db->query('select @@IDENTITY as last_id');
						$datapost['id_user'] = $query->row()->last_id;
						$this->sp_model->exec('user_unit',$datapost);
					}
				}
				redirect('user');
			}

			$this->loadview('user',['table'=>'']);
		} else {
			redirect('');
		}
	}

	public function useraktif()
	{
		$session = $this->session->userdata();
		if ($session['hakakses']=='admin') {
			$this->load->model('User_Model');
			$datapost = $this->input->post();
			if (isset($datapost['id_user'])) {
				$id_user = $datapost['id_user'];
				$aktif = $datapost['aktif'];
				$user = $this->User_Model->get($id_user);
				if ($user){
					$this->User_Model->update(['aktif'=>$aktif],['id_user'=>$id_user]);
				}
			}
			redirect('user');
		} else {
			redirect('');
		}
	}

}
