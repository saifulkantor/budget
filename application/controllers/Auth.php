<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function index()
	{
		if (isset($this->session->id_user) && $this->session->id_user > -1) {
			redirect('');
		} else {
			//$this->session->sess_destroy();
			$this->load->view('login/form_login', array('errMsg' => $this->session->flashdata('errLoginMsg')));
		}
	}

	public function login()
	{
		$this->load->model('User_model');

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$do = true;
		if ($username == '') {
			$msg = 'Username tidak boleh kosong';
			$do = false;
		}

		if ($password == '') {
			$msg = 'Password tidak boleh kosong';
			$do = false;
		}

		$timeLogin = date('YmdHis').rand(1,9999);

		if ($do) {
			// get user
			$user = $this->User_model->getbyusername($username);

			if ($user->id_user == '' or $user->id_user == null or $user->aktif!=1) {
				$msg = 'User tidak ditemukan';
				$do = false;
			} else {
				if (password_verify($password,$user->password)) {
					$newdata = array(
						'id_user' => $user->id_user,
						'username' => $user->username,
						'nama' => $user->nama,
						'email' => $user->email,
						'hakakses' => $user->hakakses,
						'id_unit' => ($user->id_unit!=NULL)?$user->id_unit:0,
						'logged_in' => TRUE,
						'timelogin' => $timeLogin,
						'authkey' => password_hash($user->id_user.$timeLogin, PASSWORD_BCRYPT),
					);
					$this->session->set_userdata($newdata);
					redirect();
				} else {
					$msg = 'Password salah';
					$do = false;
				}
			}
		}

		if (!$do) {
			$this->session->set_flashdata('errLoginMsg', $msg);

			redirect('Auth');
		}
	}

	function generateMenu($menu, $skipAkses = false) {
		$temp = '';
		$i = 1;
		$arrMenu = $arrLaporan = array();
		foreach($menu as $row) {
			$insert = false;
			if ($skipAkses) {
				$insert = true;
			} else if ($row->akses == 1) {
				$insert = true;
			}

			if ($insert == true) {
				if ($i >= 8)
					$i = 1;

				if ($temp <> $row->modul) {
					$temp = $row->modul;
				}

				$row->color = 'c'.$i;

				// apakah menu laporan ?
				$a = explode(' ', $row->title);
				if (in_array('Laporan', $a)) {
					$arrLaporan[$temp][] = $row;
				} else {
					$arrMenu[$temp][] = $row;
				}

				$i++;
			}
		}

		return array('daftarMenu' => $arrMenu, 'daftarLaporan' => $arrLaporan);
	}

	public function logout()
	{
		$this->session->sess_destroy();

		redirect();
	}

	public function cekSession() {
		echo (isset($this->session->idperusahaan) && $this->session->idperusahaan > 0) ? 1 : 0;
	}
}
