<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Realisasi extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($data='')
	{
		$this->load->model('Unit_Model');
		$this->loadview('realisasi/data',['table'=>$data]);
	}

	// Bank
	public function bank($remark)
	{
		if ($remark=='simpan') {
			$this->bank_simpan();
		} else if ($remark=='ajaxbank') {
			$this->bank_ajaxbank();
		} else {
			$this->load->model('Unit_Model');
			$this->load->model('Bank_Model');
			$this->loadview('realisasi/bank',['remark'=>$remark]);
		}
	}

	public function bank_ajaxbank()
	{
		$this->load->model('Unit_Model');
		$this->load->model('Bank_Model');
		$datapost = $this->input->post();
		echo json_encode($this->sp2_model->exec('ajaxbank',$datapost));
	}
	public function bank_simpan()
	{
		$datapost = $this->input->post();
		foreach ($datapost as $key => $value) {
			if ($value=='') $datapost[$key]=0;
		}
		$this->sp2_model->exec('bank_simpan',$datapost);
		$unit = $datapost['unit'];
		$tahun = $datapost['tahun'];
		$remark = $datapost['remark'];
		$this->session->set_flashdata('errLoginMsg', 'Data Berhasil Disimpan');
		redirect('realisasi/bank/'.$remark.'?tahun='.$tahun);
	}
	public function bank_simpanket($data='')
	{
		$datapost = $this->input->post();
		$this->sp2_model->exec('data_simpanket',$datapost);
		$unit = $datapost['unit'];
		$tahun = $datapost['tahun'];
		$this->session->set_flashdata('errLoginMsg', 'Data Berhasil Disimpan');
		redirect('data/'.$data.'?unit='.$unit.'&tahun='.$tahun);
	}
	public function bank_status($data='')
	{
		$datapost = $this->input->post();
		$this->sp2_model->exec('data_status',$datapost);
		$this->session->set_flashdata('errLoginMsg', 'Data Berhasil Disimpan');
		if (isset($datapost['unit'])&&isset($datapost['unit'])) {
			$unit = $datapost['unit'];
			$tahun = $datapost['tahun'];
			redirect('data/'.$data.'?unit='.$unit.'&tahun='.$tahun);
		}
	}



	public function laporan()
	{
		$this->load->model('Unit_Model');
		$this->loadview('realisasi/laporan');
	}
	public function rekap()
	{
		$this->load->model('Unit_Model');
		$this->loadview('realisasi/rekap',['table'=>'rekap']);
	}
	public function simpan($data='')
	{
		$this->load->model('Realisasi_Model');
		$datapost = $this->input->post();
		$sql = '';
		foreach ($datapost['gantinilai'] as $key => $value) {
			if ($value=='') { $datapost['gantinilai'][$key]=0; $value=0; }
			$idkey = explode("h",$key);
			$datanya = $this->Realisasi_Model->get($idkey[0],$idkey[1]);
			if (isset($datanya['data'])) {
				$sql.="UPDATE realisasi SET amount=$value WHERE id_perkiraan=$idkey[0] and tanggal = '$idkey[1]'; ";
			} else {
				$sql.="INSERT INTO realisasi (id_perkiraan,amount,tanggal,auth_key,locked) VALUES ($idkey[0],$value,'$idkey[1]','',0); ";
			}
		}
		if ($sql!='') $query = $this->db->query($sql);
		// $this->sp2_model->exec('data_simpan',$datapost);
		$unit = $datapost['unit'];
		$bulan = $datapost['bulan']??date('m');
		$tahun = $datapost['tahun']??date('Y');
		$this->session->set_flashdata('errLoginMsg', 'Data Berhasil Disimpan');
		redirect('realisasi/'.$data.'?unit='.$unit.'&bulan='.$bulan.'&tahun='.$tahun);
	}
	public function simpanket($data='')
	{
		$datapost = $this->input->post();
		$this->sp_model->exec('data_simpanket',$datapost);
		$unit = $datapost['unit'];
		$tahun = $datapost['tahun'];
		$this->session->set_flashdata('errLoginMsg', 'Data Berhasil Disimpan');
		redirect('realisasi/'.$data.'?unit='.$unit.'&tahun='.$tahun);
	}
	public function lockdata($data='')
	{
		$session = $this->session->userdata();
		if ($session['hakakses']=='admin') {
			$datapost = $this->input->post();
			$this->sp_model->exec('lockdata',$datapost);
			$unit = $datapost['unit'];
			$tahun = $datapost['tahun'];
			$this->session->set_flashdata('errLoginMsg', 'Data Berhasil Disimpan');
		}
		redirect('realisasi/'.$data.'?unit='.$unit.'&tahun='.$tahun);
	}
	public function status($data='')
	{
		$datapost = $this->input->post();
		$this->sp_model->exec('data_status',$datapost);
		$this->session->set_flashdata('errLoginMsg', 'Data Berhasil Disimpan');
		if (isset($datapost['unit'])&&isset($datapost['unit'])) {
			$unit = $datapost['unit'];
			$tahun = $datapost['tahun'];
			redirect('realisasi/'.$data.'?unit='.$unit.'&tahun='.$tahun);
		}
	}
}
