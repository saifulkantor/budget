<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($data='')
	{
		$this->load->model('Unit_Model');
		$this->loadview('table/data',['table'=>$data]);
	}
	public function laporan()
	{
		$this->load->model('Unit_Model');
		$this->loadview('table/laporan');
	}
	public function rekap()
	{
		$this->load->model('Unit_Model');
		$this->loadview('table/rekap',['table'=>'rekap']);
	}
	public function bank($remark)
	{
		if ($remark=='simpan') {
			$this->simpan();
		} else if ($remark=='ajaxbank') {
			$this->ajaxbank();
		} else {
			$this->load->model('Unit_Model');
			$this->load->model('Bank_Model');
			$this->loadview('table/bank',['remark'=>$remark]);
		}
	}
	public function simpan($data='')
	{
		$datapost = $this->input->post();
		foreach ($datapost as $key => $value) {
			if ($value=='') $datapost[$key]=0;
		}
		$this->sp_model->exec('data_simpan',$datapost);
		$unit = $datapost['unit'];
		$tahun = $datapost['tahun'];
		$this->session->set_flashdata('errLoginMsg', 'Data Berhasil Disimpan');
		redirect('data/'.$data.'?unit='.$unit.'&tahun='.$tahun);
	}
	public function simpanket($data='')
	{
		$datapost = $this->input->post();
		$this->sp_model->exec('data_simpanket',$datapost);
		$unit = $datapost['unit'];
		$tahun = $datapost['tahun'];
		$this->session->set_flashdata('errLoginMsg', 'Data Berhasil Disimpan');
		redirect('data/'.$data.'?unit='.$unit.'&tahun='.$tahun);
	}
	public function lockdata($data='')
	{
		$session = $this->session->userdata();
		if ($session['hakakses']=='admin') {
			$datapost = $this->input->post();
			$this->sp_model->exec('lockdata',$datapost);
			$unit = $datapost['unit'];
			$tahun = $datapost['tahun'];
			$this->session->set_flashdata('errLoginMsg', 'Data Berhasil Disimpan');
		}
		redirect('data/'.$data.'?unit='.$unit.'&tahun='.$tahun);
	}
	public function status($data='')
	{
		$datapost = $this->input->post();
		$this->sp_model->exec('data_status',$datapost);
		$this->session->set_flashdata('errLoginMsg', 'Data Berhasil Disimpan');
		if (isset($datapost['unit'])&&isset($datapost['unit'])) {
			$unit = $datapost['unit'];
			$tahun = $datapost['tahun'];
			redirect('data/'.$data.'?unit='.$unit.'&tahun='.$tahun);
		}
	}
}
