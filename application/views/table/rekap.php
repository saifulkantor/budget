<?php
$tahun = isset($_GET['tahun'])?$_GET['tahun']:date('Y');

$data = $this->sp_model->exec('rekap',['tahun'=>$tahun])['rows'];
$datakredit = $this->sp_model->exec('kredit',['tahun'=>$tahun])['rows'];
$datapemhutang = $this->sp_model->exec('hp3',['tahun'=>$tahun])['rows'];
$unitall = $this->Unit_Model->getAll()['data'];
$totallabarugi['ket']='TOTAL';
?>
<div>
    <div class="card strpied-tabled-with-hover">
        <?php /*<div class="card-header ">
            <h4 class="card-title">Rekap</h4>
        </div> */ ?>
        <form action="" method="get">
          <div class="col-xs-6 col-md-4" style="float:left">
            <!-- <label>Year</label> -->
            <input type="number" class="form-control" name="tahun" onchange="this.form.submit()" value="<?=$tahun?>" >
          </div>
          <div class="col-xs-6 col-md-4" style="float:right;text-align:right">
            <!-- <label style="width:100%">&nbsp;</label> -->
            <button type="button" onclick="fnExcelReport('tablerekap','Rekap Data (<?=$tahun?>)')" class="btn btn-primary" style="cursor:pointer"> Export Excel</button>
          </div>
        </form>
        <div class="col-md-12" >
        <div <?php /*class="card-body table-full-width table-responsive"*/ ?>>
            <table id="tablerekap" class="table table-hover table-striped table-bordered table-freeze tablerekapdata" style="display:block;overflow-x:auto;">
                <thead><tr>
                  <?php foreach ($data[0] as $key => $value) {
                    if ($key=='ket') echo '<th>Keterangan</th>'; else if ($key!='doc_id'&&$key!='unit'&&$key!='ket'){
                      echo '<th>'.lengkapibulan($key).'</th>';
                      $totallabarugi[$key]=0;
                    }
                  } ?>
                  <th style="display:none">TOTAL</th></tr>
                </thead>
                <tbody>
                  <?php foreach ($data as $key => $value) {
                    if ($value->ket=='A.Rev' || $value->ket=='B.Cost') {
                      $kelas = strtolower($value->ket); $tambahan='';
					  $kelas .= ($value->ket=='A.Rev')?' rev':' cost';
                    } else {
                      $kelas = 'namaunit'; $tambahan=' onclick="kelaporan('.$value->doc_id.')" style="cursor:pointer"';
                    }
                    echo '<tr class="'.$kelas.'" id="tr'.$key.'" data-iddoc="'.$value->doc_id.'"'.$tambahan.'>';
                      $total=0;
                      foreach ($data[$key] as $key2 => $value2) {
                        if ($key2!='doc_id'&&$key2!='unit') {
                          if ($key2=='ket') echo ($value2!='-')?(($value->ket=='A.Rev' || $value->ket=='B.Cost')?'<td class="datapentingtabel"> &nbsp; &nbsp; '.$value2.'</td>':'<td class="datapentingtabel">'.$value2.'</td>'):'<td class="datapentingtabel">'.$value->unit.'</td>'; else {
                            echo '<td style="text-align:right" id="td'.$key.$key2.'" data-nilai="'.$value2.'">'.number_format($value2,2,',','.').'</td>';
                            $total+=$value2;
                            if ($value->ket!='A.Rev'&&$value->ket!='B.Cost') $totallabarugi[$key2]+=$value2;
                          }
                        }
                      }
                    echo '<td style="display:none">'.number_format($total,2,',','.').'</td></tr>';
                  } ?>
                  <?php
                    echo '<tr id="trtotal" class="total" data-iddoc="0">';
                    $total=0;
                      foreach ($totallabarugi as $key2 => $value2) {
                        if ($key2!='doc_id') {
                          if ($key2=='ket') echo '<td>LABA RUGI</td>'; else {
                            echo '<td style="text-align:right" id="td'.$key.$key2.'" data-nilai="'.$value2.'">'.number_format($value2,2,',','.').'</td>';
                            $total+=$value2;
                          }
                        }
                      }
                    echo '<td style="display:none">'.number_format($total,2,',','.').'</td></tr>';
                     ?>
                  <?php foreach ($datakredit as $key => $value) {
                    $total=0;
                    echo '<tr id="tr'.$key.'" class="kredit">';
                      foreach ($datakredit[$key] as $key2 => $value2) {
                        if ($key2!='doc_id') {
                          if ($key2=='kelompok') echo '<td>KREDIT BANK</td>'; else {
                            echo '<td style="text-align:right" id="td'.$key.$key2.'" data-nilai="'.$value2.'">'.number_format($value2,2,',','.').'</td>';
                            $totallabarugi[$key2]+=$value2;
                            $total+=$value2;
                          }
                        }
                      }
                    echo '<td style="display:none">'.number_format($total,2,',','.').'</td></tr>';
                  } ?>
                  <?php foreach ($datapemhutang as $key => $value) {
                    $total=0;
                    echo '<tr id="tr'.$key.'" class="hp3">';
                      foreach ($datapemhutang[$key] as $key2 => $value2) {
                        if ($key2!='doc_id') {
                          if ($key2=='kelompok') echo '<td>PEMBAYARAN HUTANG</td>'; else {
                            echo '<td style="text-align:right" id="td'.$key.$key2.'" data-nilai="'.$value2.'">'.number_format($value2,2,',','.').'</td>';
                            $totallabarugi[$key2]+=$value2;
                            $total+=$value2;
                          }
                        }
                      }
                    echo '<td style="display:none">'.number_format($total,2,',','.').'</td></tr>';
                  } ?>
                  <?php
                    echo '<tr id="trtotal" class="deviasi" data-iddoc="0">';
                    $total=0;
                      foreach ($totallabarugi as $key2 => $value2) {
                        if ($key2!='doc_id') {
                          if ($key2=='ket') echo '<td>DEVIASI</td>'; else {
                            echo '<td style="text-align:right" id="td'.$key.$key2.'" data-nilai="'.$value2.'">'.number_format($value2,2,',','.').'</td>';
                            $total+=$value2;
                          }
                        }
                      }
                    echo '<td style="display:none">'.number_format($total,2,',','.').'</td></tr>';
                     ?>
                </tbody>
            </table>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
function kelaporan(id_unit=1) {
  location.href='<?=base_url()?>laporan?tahun=<?=$tahun?>&unit='+id_unit;
}
window.addEventListener('DOMContentLoaded', (event) => {
  $('.judulhalaman').html('Rekap Budget');
  
});
</script>
