<?php
$tahun = isset($_GET['tahun'])?$_GET['tahun']:date('Y');
$unit = isset($_GET['unit'])?$_GET['unit']:1;
$namaunit='';
$filtertambahan = ($unit>0)?' AND A.id_unit='.$unit:'';
$data = $this->sp_model->exec('bank',['filtertambahan'=>$filtertambahan])['rows'];
$unitall = $this->Unit_Model->getAll()['data'];
?>
<div class="col-md-12">
    <div class="card strpied-tabled-with-hover">
        <div class="card-header ">
            <h4 class="card-title">Tabel Bank</h4>
        </div>
        <div class="formutama" >
          <form action="" method="get">
            <div class="col-xs-6 col-md-4" style="float:left">
              <label>Unit</label>
              <select class="form-control" name="unit" onchange="this.form.submit()">
                <option value="0"> -- Semua Unit -- </option>
                <?php foreach ($unitall as $key => $value) {
                  $selected=($unit==$value['doc_id'])?' selected':'';
                  $namaunit=($unit==$value['doc_id'])?$value['unit']:$namaunit;
                  echo '<option value="'.$value['doc_id'].'"'.$selected.'>'.$value['unit'].'</option>';
                } ?>
              </select>
            </div>
            <div class="col-xs-6 col-md-4" style="float:right;text-align:right">
              <label style="width:100%">&nbsp;</label>
              <button type="button" onclick="fnExcelReport('tabledata','Bank <?=$namaunit?>')" class="btn btn-primary" style="cursor:pointer"> Export Excel</button>
            </div>
          </form>
          <div class="clearfix"></div>
          <div class="card-body table-full-width table-responsive">
              <div class="col-xs-12">
                <button type="button" onclick="tambahketerangan()" class="btn btn-primary" style="cursor:pointer"> Tambah</button>
                <button type="button" onclick="tampilarsip(true)" class="btn btn-primary" style="cursor:pointer"> Arsip</button>
              </div>
              <table id="tabledata" class="tableedit table table-hover table-striped table-bordered" style="display:block;overflow-x:auto;">
                  <thead><tr>
                    <td></td><td>Bank</td><td>Unit</td><td>Amount</td></tr>
                  </thead>
                  <tbody>
                    <?php $total=0;
                    foreach ($data as $key => $value) {
                      echo '<tr'.(($value->bank=='TOTAL')?' class="total"':'').' id="tr'.$key.'" data-idper="'.$value->id_bank.'">';
                      echo '<td><i class="nc-icon nc-bullet-list-67" style="cursor:pointer" onclick="ubahnilai('.$key.')"></i> <i class="fa fa-trash" style="cursor:pointer" onclick="hapusdata('.$key.')"></i></td>';
                        foreach ($data[$key] as $key2 => $value2) {
                          if ($key2!='id_bank') {
                            if ($key2=='bank'||$key2=='unit') echo '<td data-ket="'.$value2.'">'.$value2.'</td>'; else if($key2=='amount') {
                              echo '<td style="text-align:right" id="td'.$key.$key2.'" data-nilai="'.$value2.'">'.number_format($value2,2,',','.').'</td>';
                              $total+=$value2;
                            }
                          }
                        }
                      echo '</tr>';
                    }
                    echo '<tr class="total"><td></td><td colspan="2" style="display:table-cell;">TOTAL</td><td>'.number_format($total,2,',','.').'</td></tr>'; ?>
                  </tbody>
              </table>
          </div>
        </div>
        <div class="arsip" style="display:none">
          <div class="card-body table-full-width table-responsive">
            <div class="col-xs-12">
              <button type="button" onclick="tampilarsip(false)" class="btn btn-primary" style="cursor:pointer"> Kembali</button>
            </div>
            <table id="tabledata" class="tableedit table table-hover table-striped table-bordered" style="display:block;overflow-x:auto;">
              <thead><tr><th>Keterangan</th></tr></thead>
              <tbody>
                <?php foreach ($perkiraanarsip as $key => $value) {
                  echo '<tr id="trarsip'.$key.'" data-idper="'.$value['id_bank'].'"><td data-ket="'.$value['bank'].'">'.$value['bank'].' <i class="fa fa-undo" style="cursor:pointer" onclick="archive('.$key.')"></i></td></tr>';
                } ?>
              </tbody>
            </table>
          </div>
        </div>

    </div>
</div>

<div class="modal fade modal-primary" id="ubahnilai" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="-webkit-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);">
        <div class="modal-content">
          <form action="<?=base_url()?>data/<?=$table?>/simpan" method="post">
            <div class="modal-header justify-content-center">
                Ubah Nilai
            </div>
            <div class="modal-body">
              <input type="hidden" name="aksi" value="ubahnilai">
              <input type="hidden" name="tahun" value="<?=$tahun?>">
              <input type="hidden" name="unit" value="<?=$unit?>">
              <input type="hidden" name="id_bank" class="id_bank" value="">
              <table>
                <?php
                foreach (BULAN as $key => $bln) {
                  echo '<tr><td><label>'.BULAN_FULL[$key].'</label></td><td><input id="ubahnilai'.$bln.'" type="number" step="0.00000000000000001" class="form-control" name="'.$bln.'" value="" placeholder="0" /></td></tr>';
                }
                ?>
              </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" style="cursor:pointer">Simpan</button>
            </div>
          </form>
        </div>
    </div>
</div>


<div class="modal fade modal-primary" id="ubahketerangan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="-webkit-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);">
        <div class="modal-content">
          <form action="<?=base_url()?>data/<?=$table?>/simpanket" method="post">
            <div class="modal-header justify-content-center">
                Ubah Keterangan
            </div>
            <div class="modal-body">
              <input type="hidden" name="aksi" value="ubahketerangan">
              <input type="hidden" name="tahun" value="<?=$tahun?>">
              <input type="hidden" name="unit" value="<?=$unit?>">
              <input type="hidden" name="id_bank" class="id_bank" value="">
              <div class="col-xs-12">
                <label>Keterangan</label>
                <input type="text" class="form-control keterangan" name="bank" value="" >
              </div>
              <div class="col-xs-12">
                <label>Kelompok</label>
                <select class="form-control" name="id_kelompok">
                  <?php foreach ($kelompokall as $key => $value) {
                    $selected=($table_id==$value['doc_id'])?' selected':'';
                    echo '<option value="'.$value['doc_id'].'"'.$selected.'>'.$value['kelompok'].'</option>';
                  } ?>
                </select>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" style="cursor:pointer">Simpan</button>
            </div>
          </form>
        </div>
    </div>
</div>

<div class="modal fade modal-primary" id="hapusdata" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="-webkit-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);">
        <div class="modal-content">
          <form action="<?=base_url()?>data/<?=$table?>/status" method="post">
            <div class="modal-header justify-content-center">
                Apakah Anda Yakin ingin Mengarsipkan Keterangan ini dan semua datanya ?
            </div>
            <div class="modal-body">
              <input type="hidden" name="aksi" value="ubahketerangan">
              <input type="hidden" name="tahun" value="<?=$tahun?>">
              <input type="hidden" name="unit" value="<?=$unit?>">
              <input type="hidden" name="id_bank" class="id_bank" value="">
              <input type="hidden" name="status" class="status" value="0">
              <h3></h3>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" style="cursor:pointer">Iya</button>
            </div>
          </form>
        </div>
    </div>
</div>

<div class="modal fade modal-primary" id="archive" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="-webkit-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);">
        <div class="modal-content">
          <form action="<?=base_url()?>data/<?=$table?>/status" method="post">
            <div class="modal-header justify-content-center">
                Apakah Anda Yakin ingin Mengembalikan Keterangan ini dan semua datanya ?
            </div>
            <div class="modal-body">
              <input type="hidden" name="aksi" value="ubahketerangan">
              <input type="hidden" name="tahun" value="<?=$tahun?>">
              <input type="hidden" name="unit" value="<?=$unit?>">
              <input type="hidden" name="id_bank" class="id_bank" value="">
              <input type="hidden" name="status" class="status" value="1">
              <h3></h3>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" style="cursor:pointer">Iya</button>
            </div>
          </form>
        </div>
    </div>
</div>

<script type="text/javascript">
  function ubahnilai(key=0) {
    $('#ubahnilai').modal();
    $('#ubahnilai .modal-header').html($('#tr'+key+' td:nth-child(2)').attr('data-ket'));
    $('#ubahnilai .id_bank').val($('#tr'+key).attr('data-idper'));
    <?php foreach (BULAN as $key => $bln) {
      echo "$('#ubahnilai".$bln."').val($('#td'+key+'".$bln."').attr('data-nilai'));";
    } ?>
  }
  function ubahketerangan(key=0) {
    $('#ubahketerangan').modal();
    $('#ubahketerangan .id_bank').val($('#tr'+key).attr('data-idper'));
    $('#ubahketerangan .modal-header').html('Ubah Keterangan');
    $('#ubahketerangan .keterangan').val($('#tr'+key+' td:nth-child(2)').attr('data-ket'));
  }
  function tambahketerangan() {
    $('#ubahketerangan').modal();
    $('#ubahketerangan .id_bank').val(<?=$table_id?>);
    $('#ubahketerangan .modal-header').html('Tambah Keterangan');
    $('#ubahketerangan .keterangan').val('');
  }
  function hapusdata(key=0) {
    $('#hapusdata').modal();
    $('#hapusdata .id_bank').val($('#tr'+key).attr('data-idper'));
    $('#hapusdata .modal-body h3').html($('#tr'+key+' td:nth-child(2)').attr('data-ket'));
  }
  function archive(key=0) {
    $('#archive').modal();
    $('#archive .id_bank').val($('#trarsip'+key).attr('data-idper'));
    $('#archive .modal-body h3').html($('#trarsip'+key+' td:first-child').attr('data-ket'));
  }
  function tampilarsip(benar) {
    if (benar) {$('.formutama').hide();$('.arsip').show();} else {$('.arsip').hide();$('.formutama').show();}
  }
</script>
