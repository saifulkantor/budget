<?php
$tahun = isset($_GET['tahun'])?$_GET['tahun']:date('Y');
$unit = isset($_GET['unit'])?$_GET['unit']:1;
$namaunit='';
$data = $this->sp_model->exec($remark,['tahun'=>$tahun])['rows'];
$unitall = $this->Unit_Model->getAll()['data'];
$bankall = $this->Bank_Model->getAll()['data'];
?>
<div>
    <div class="card strpied-tabled-with-hover">
        <?php /*<div class="card-header ">
            <h4 class="card-title"><?=ucfirst($remark)?> Bank</h4>
        </div> */ ?>
        <div class="formutama col-md-12" >
          <form action="" method="get">
            <div class="col-xs-6 col-md-4" style="float:left">
              <!-- <label>Year</label> -->
              <input type="number" class="form-control" name="tahun" onchange="this.form.submit()" value="<?=$tahun?>" >
            </div>
            <div class="col-xs-6 col-md-4" style="float:left">
              <!-- <label>Act</label> -->
              <button type="button" onclick="tambahnilai()" class="btn btn-primary" style="cursor:pointer"> Tambah</button>
            </div>

            <div class="col-xs-6 col-md-4" style="float:right;text-align:right">
              <!-- <label style="width:100%">&nbsp;</label> -->
              <button type="button" onclick="fnExcelReport('tabledata','<?=ucfirst($remark)?> Bank <?=$namaunit?> (<?=$tahun?>)')" class="btn btn-primary" style="cursor:pointer"> Export Excel</button>
            </div>
          </form>
          <div class="clearfix"></div>
          <div class="card-body table-full-width table-responsive">
              <table id="tabledata" class="tableedit tabletdhover table table-hover table-striped table-bordered table-freeze" style="display:block;overflow-x:auto;">
                  <thead><tr>
                    <?php foreach ($data[0] as $key => $value) {
                      if ($key=='id_bank'||$key=='id_unit') echo ''; else if ($key=='bank') echo '<th style="z-index: 12!important;">Bank</th>'; else if ($key=='unit') echo '<th style="z-index: 10!important;">Unit</th>'; else if ($key=='YTD') echo '<th>Total</th>'; else echo '<th id="th'.(array_search ($key, BULAN)+1).'">'.lengkapibulan($key).'</th>';
                    } ?>
                  </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($data as $key => $value) {
                      echo '<tr'.(($value->unit=='TOTAL')?' class="total"':'').' id="tr'.$key.'" data-id_bank="'.$value->id_bank.'" data-id_unit="'.$value->id_unit.'">';
                        foreach ($data[$key] as $key2 => $value2) {
                          if ($key2!='id_bank'&&$key2!='id_unit') {
                            if ($key2=='bank'||$key2=='unit') {
                              if ($value2!=NULL) {
                                $colspan=($value2=='TOTAL')?' colspan="2"':'';
                                echo '<td data-ket="'.$value2.'"'.$colspan.' style="width:auto;display:table-cell;background:white">'.$value2.'</td>';
                              }
                            } else {
                              $onclick=($value->unit!='TOTAL'&&$key2!='YTD')?'onclick="detailnilai('.$key.',\''.$key2.'\')"':'';
                              echo '<td id="td'.$key.$key2.'" data-bulan="'.(array_search ($key2, BULAN)+1).'" data-nilai="'.$value2.'" style="text-align:right;cursor:pointer;width:auto" '.$onclick.'>'.number_format($value2,2,',','.').'</td>';
                            }
                          }
                        }
                      echo '</tr>';
                    } ?>
                  </tbody>
              </table>
          </div>
        </div>
        <div class="formdetail col-md-12" style="display:none">
          <button type="button" onclick="tampildetail(false)" class="btn btn-primary" style="cursor:pointer;float:left;padding:2px 5px;">&lt;&lt;</button>
          <h4 style="float:left;padding: 0px 10px;margin:0;"></h4>
          <button type="button" onclick="tambahnilai()" class="btn btn-primary tambah" style="cursor:pointer;float:left;padding:2px 5px;"> Tambah</button>
          <div style="clear:both"></div>
          <div class="card-body table-full-width table-responsive">
            <table id="tabledetail" class="tableedit tabletdhover table table-hover table-striped table-bordered table-freeze table-single" style="display:block;overflow-x:auto;width: min-content;">
              <thead><tr><th></th><th>Tanggal</th><th>Amount</th></tr></thead>
              <tbody></body>
            </table>
          </div>
        </div>

        <div class="arsip" style="display:none">
          <div class="card-body table-full-width table-responsive">
            <div class="col-xs-12">
              <button type="button" onclick="tampilarsip(false)" class="btn btn-primary" style="cursor:pointer"> Kembali</button>
            </div>
            <table id="tabledata" class="tableedit table table-hover table-striped table-bordered" style="display:block;overflow-x:auto;">
              <thead><tr><th>Keterangan</th></tr></thead>
              <tbody>
                <?php foreach ($perkiraanarsip as $key => $value) {
                  echo '<tr id="trarsip'.$key.'" data-idper="'.$value['id_bank'].'"><td data-ket="'.$value['nama_perkiraan'].'">'.$value['nama_perkiraan'].' <i class="fa fa-undo" style="cursor:pointer" onclick="archive('.$key.')"></i></td></tr>';
                } ?>
              </tbody>
            </table>
          </div>
        </div>

    </div>
</div>

<div class="modal fade modal-primary" id="tambahnilai" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="-webkit-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);">
        <div class="modal-content">
          <form action="<?=base_url()?>bank/simpan" method="post">
            <div class="modal-header justify-content-center">
                Tambah Data
            </div>
            <div class="modal-body">
              <input type="hidden" name="aksi" value="ubahnilai">
              <input type="hidden" name="tahun" value="<?=$tahun?>">
              <input type="hidden" class="doc_id" name="doc_id" value="0">
              <input type="hidden" class="remark" name="remark" value="<?=$remark?>">
              <div class="clearfix"></div>
              <div class="col-xs-6 col-md-4" style="float:left">
                <label>Bank</label>
                <select class="form-control" name="id_bank">
                  <?php foreach ($bankall as $key => $value) {
                    echo '<option value="'.$value['doc_id'].'">'.$value['bank'].'</option>';
                  } ?>
                </select>
              </div>
              <div class="col-xs-6 col-md-4" style="float:left">
                <label>Unit</label>
                <select class="form-control" name="id_unit">
                  <?php foreach ($unitall as $key => $value) {
                    echo '<option value="'.$value['doc_id'].'">'.$value['unit'].'</option>';
                  } ?>
                </select>
              </div>
              <div class="clearfix"></div>
              <div style="width:100%">
                <label>Tanggal</label>
                <input id="tambahtgl" type="date" min="" max="" class="form-control" name="tgl" value="<?=date('Y-m-d')?>" required />
              </div>
              <div style="width:100%">
                <label>Amount</label>
                <input id="tambahamount" type="number" step="0.00000000000000001" class="form-control" name="amount" value="" placeholder="0" required />
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" style="cursor:pointer">Simpan</button>
            </div>
          </form>
        </div>
    </div>
</div>

<div class="modal fade modal-primary" id="ubahnilai" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="-webkit-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);">
        <div class="modal-content">
          <form action="<?=base_url()?>bank/simpan" method="post">
            <div class="modal-header justify-content-center">
                Ubah Data
            </div>
            <div class="modal-body">
              <input type="hidden" name="aksi" value="ubahnilai">
              <input type="hidden" class="tahun" name="tahun" value="<?=$tahun?>">
              <input type="hidden" class="id_unit" name="id_unit" value="<?=$unit?>">
              <input type="hidden" class="id_bank" name="id_bank" value="">
              <input type="hidden" class="doc_id" name="doc_id" value="">
              <input type="hidden" class="remark" name="remark" value="<?=$remark?>">
              <div class="clearfix"></div>
              <div class="form-group">
                <label>Tanggal</label>
                <input id="ubahtgl" type="date" min="" max="" class="form-contol" name="tgl" value="" />
              </div>
              <div class="form-group">
                <label>Amount</label>
                <input id="ubahamount" type="number" step="0.00000000000000001" class="form-control" name="amount" value="" placeholder="0" />
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" style="cursor:pointer">Simpan</button>
            </div>
          </form>
        </div>
    </div>
</div>

<div class="modal fade modal-primary" id="archive" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="-webkit-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);">
        <div class="modal-content">
          <form action="<?=base_url()?>bank/status" method="post">
            <div class="modal-header justify-content-center">
                Apakah Anda Yakin ingin Mengembalikan Keterangan ini dan semua datanya ?
            </div>
            <div class="modal-body">
              <input type="hidden" name="aksi" value="ubahketerangan">
              <input type="hidden" name="tahun" value="<?=$tahun?>">
              <input type="hidden" name="unit" value="<?=$unit?>">
              <input type="hidden" name="id_bank" class="id_bank" value="">
              <input type="hidden" name="status" class="status" value="1">
              <h3></h3>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" style="cursor:pointer">Iya</button>
            </div>
          </form>
        </div>
    </div>
</div>

<script type="text/javascript">
  function tambahnilai(){
    $('#tambahnilai').modal();
  }
  function ubahnilai(bulan,id_bank,unit,tahun,tgl,amount,doc_id) {
    $('#ubahtgl').attr('min',formatDate(new Date(tahun,(bulan-1))));
    $('#ubahtgl').attr('max',formatDate(new Date(tahun,bulan,0)));
    $('#ubahtgl').val(tgl);
    $('#ubahamount').val(amount);
    $('#ubahnilai .id_bank').val(id_bank);
    $('#ubahnilai .id_unit').val(unit);
    $('#ubahnilai .doc_id').val(doc_id);
    $('#ubahnilai').modal();
    $('#ubahnilai .modal-header').html($('.formdetail>h4').html());
  }
  function detailnilai(key=0,bln='') {
    var bulan = $('#td'+key+bln).attr('data-bulan');
    var id_bank = $('#tr'+key).attr('data-id_bank');
    var unit = $('#tr'+key).attr('data-id_unit');
    dataajax(bulan,id_bank,unit,<?=$tahun?>);
    $('.formdetail .btn.tambah').attr('onclick','ubahnilai('+bulan+','+id_bank+','+unit+',<?=$tahun?>,\''+formatDate(new Date(<?=$tahun?>,(bulan-1)))+'\',0,0);');
    tampildetail();
    $('.formdetail>h4').html($('#tr'+key+' td:nth-child(1)').attr('data-ket')+' - '+$('#tr'+key+' td:nth-child(2)').attr('data-ket')+' ('+$('#th'+bulan).html()+' <?=$tahun?>) : '+$('#td'+key+bln).html());
  }
  function ubahketerangan(key=0) {
    $('#ubahketerangan').modal();
    $('#ubahketerangan .id_bank').val($('#tr'+key).attr('data-idper'));
    $('#ubahketerangan .modal-header').html('Ubah Keterangan');
    $('#ubahketerangan .keterangan').val($('#tr'+key+' td:nth-child(2)').attr('data-ket'));
  }
  function hapusdata(key=0) {
    $('#hapusdata').modal();
    $('#hapusdata .id_bank').val($('#tr'+key).attr('data-idper'));
    $('#hapusdata .modal-body h3').html($('#tr'+key+' td:nth-child(2)').attr('data-ket'));
  }
  function archive(key=0) {
    $('#archive').modal();
    $('#archive .id_bank').val($('#trarsip'+key).attr('data-idper'));
    $('#archive .modal-body h3').html($('#trarsip'+key+' td:first-child').attr('data-ket'));
  }
  function tampilarsip(benar=true) {
    if (benar) {$('.formutama').hide();$('.arsip').show();} else {$('.arsip').hide();$('.formutama').show();}
  }
  function tampildetail(benar=true) {
    if (benar) {$('.formutama').hide();$('.formdetail').show();} else {$('.formdetail').hide();$('.formutama').show();}
  }
  function dataajax(bulan,id_bank,unit,tahun){
    $('#tabledetail tbody').html('');
    var request = $.ajax({
      url: "<?=base_url()?>bank/ajaxbank",
      method: "POST",
      data: {
        'bulan' : bulan,
        'id_bank' : id_bank,
        'unit' : unit,
        'tahun' : tahun,
      },
      dataType: "json"
    });

    request.done(function( datahasil ) {
      var tabel='';
      hasil=datahasil['rows'];
      $.each(hasil,function(index,value){
        // tabel+='<tr><td><input type="date" min="'+formatDate(new Date(tahun,(bulan-1)))+'" max="'+formatDate(new Date(tahun,bulan,0))+'" class="form-contol" value="'+value['tgl']+'" /></td><td><input id="ubahnilai" type="number" step="0.00000000000000001" class="form-control" name="bulan" value="'+value['amount']+'" placeholder="0" /></td></tr>'
        tabel+='<tr><td><i class="nc-icon nc-ruler-pencil" style="cursor:pointer" onclick="ubahnilai('+bulan+','+id_bank+','+unit+','+tahun+',\''+value['tgl']+'\',\''+value['amount']+'\','+value['doc_id']+')"></i></td><td>'+value['tgl']+'</td><td>'+value['amount'].replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</td></tr>'
      });
      $('#tabledetail tbody').html(tabel);
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
    });

  }

  function fixedtable() {
    for (var i = 2; i <=2 ; i++) {
      $('.table-freeze:not(.table-single) tr:not(.total) td:nth-child('+i+'),.table-freeze:not(.table-single) tr th:nth-child('+i+')').css('left',parseInt($('.table-freeze tr:not(.total) td:nth-child('+(i-1)+')').css('left'))+parseInt($('.table-freeze tr:not(.total) td:nth-child('+(i-1)+')').outerWidth())+'px');
      $('.table-freeze:not(.table-single) tr:not(.total) td:nth-child('+i+')').css('z-index','9');
    }
  }

  window.addEventListener('DOMContentLoaded', (event) => {
    fixedtable();
    $('.judulhalaman').html('<?=ucfirst($remark)?> Bank');
  });

</script>
