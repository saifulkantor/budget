<?php
// $idkelompok = array(
//   'revenue' => 1,
//   'fixed_cost' => 2,
//   'variable_cost' => 3,
//   'fixed_cost_tahunan' => 4,
//   'bank_dan_kendaraan' => 5,
//   'capex' => 6,
//   'csr' => 7,
//   'Bank' => 8,
//   'Pihak3' => 9,
//   'project' => 1009
// );
$session = $this->session->userdata();
if ($session['hakakses']=='user') {
  $_GET['unit']=$session['id_unit'];
}
$tahun = isset($_GET['tahun'])?$_GET['tahun']:date('Y');
$unit = isset($_GET['unit'])?$_GET['unit']:1;
$namaunit='';
$data = $this->sp_model->exec($table,['tahun'=>$tahun,'unit'=>$unit])['rows'];
// $datalocked = $this->sp_model->exec('checklock',['tahun'=>$tahun,'unit'=>$unit,'id_kelompok'=>$idkelompok[$table]])['rows'][0]->locked;
$datalocked = $this->sp_model->exec('checklock',['tahun'=>$tahun,'unit'=>$unit])['rows'][0]->locked;
$unitall = $this->Unit_Model->getAll()['data'];
$perkiraanarsip = $this->Perkiraan_Model->getArsip($table_id,$unit)['data'];
$kelompokall = $this->Kelompok_Model->getAll()['data'];
$totalsemua['ket']='TOTAL';
?>
<div>
    <div class="card strpied-tabled-with-hover">
        <?php /*<div class="card-header ">
            <h4 class="card-title"><?=ucwords(str_replace('_',' ',str_replace('_dan_',' & ',$table)))?></h4>
        </div> */ ?>
        <div class="formutama col-md-12" >
          <form action="" method="get">
            <div class="col-xs-6 col-md-4" style="float:left">
              <!-- <label>Unit</label> -->
              <select class="form-control" name="unit" onchange="this.form.submit()">
                <?php foreach ($unitall as $key => $value) {
                  $selected=($unit==$value['doc_id'])?' selected':'';
                  $namaunit=($unit==$value['doc_id'])?$value['unit']:$namaunit;
                  echo '<option value="'.$value['doc_id'].'"'.$selected.'>'.$value['unit'].'</option>';
                } ?>
              </select>
            </div>
            <div class="col-xs-6 col-md-4" style="float:left">
              <!-- <label>Year</label> -->
              <input type="number" class="form-control" name="tahun" onchange="this.form.submit()" value="<?=$tahun?>" >
            </div>
            <div class="col-xs-3 col-md-2" style="float:left;">
            <?php if (!$datalocked) { ?>
              <!-- <label>Act</label><br/> -->
            <button type="button" onclick="tambahketerangan()" class="btn btn-primary btn-fill" style="cursor:pointer"><i class="nc-icon nc-simple-add"></i></button>
            <button type="button" onclick="tampilarsip(true)" class="btn btn-primary btn-fill" style="cursor:pointer"><i class="nc-icon nc-attach-87"></i></button>
            <?php }  ?>
            </div>
            <div class="col-xs-3 col-md-2" style="float:left;text-align:right">
              <!-- <label style="width:100%">&nbsp;</label> -->
              <button type="button" onclick="fnExcelReport('tabledata','<?=ucwords(str_replace('_',' ',str_replace('_dan_',' & ',$table)))?> <?=$namaunit?> (<?=$tahun?>)')" class="btn btn-primary" style="cursor:pointer"> Export Excel</button>
            </div>
          </form>
          <div class="card-body table-full-width table-responsive">
              <div class="col-xs-12">
                <?php /*// if ($session['hakakses']=='admin') {
                if (!$datalocked) { ?>
                <button type="button" onclick="tambahketerangan()" class="btn btn-primary btn-fill" style="cursor:pointer"><i class="nc-icon nc-simple-add"></i></button>
                <button type="button" onclick="tampilarsip(true)" class="btn btn-primary btn-fill" style="cursor:pointer"><i class="nc-icon nc-attach-87"></i></button>
              <?php } */ ?>
              </div>
			  <?php if (isset($data[0])) { ?>
              <table id="tabledata" class="tableedit table table-hover table-striped table-bordered table-freeze" style="display:block;overflow-x:auto;">
                  <thead><tr>
                    <?php
					foreach ($data[0] as $key => $value) {
                      if ($key=='id_perkiraan') echo ''; else if ($key=='nama_perkiraan') echo '<th style="z-index:12!important"></th><th style="z-index:12!important">Keterangan</th>'; else {
                        $idx = array_search(ucfirst($key),BULAN);
                        echo '<th><a href="'.base_url().'realisasi/'.$table.'?unit='.$unit.'&bulan='.($idx+1).'&tahun='.$tahun.'" style="color:white;display:block">'.lengkapibulan($key).'</a></th>';
                        $totalsemua[$key]=0;
                    }
                    } ?>
                  <th style="display:none">TOTAL</th></tr>
                  </thead>
                  <tbody>
                    <?php foreach ($data as $key => $value) {
                      echo '<tr'.(($value->nama_perkiraan=='TOTAL')?' class="total"':'').'  title="'.$value->nama_perkiraan.'" id="tr'.$key.'" data-idper="'.$value->id_perkiraan.'">';
                      $total=0;
                        foreach ($data[$key] as $key2 => $value2) {
                          if ($key2!='id_perkiraan') {
                            $tombol=($value2!='TOTAL')?(($datalocked)?'<i class="fa fa-lock"></i>':'<i class="nc-icon nc-ruler-pencil" style="cursor:pointer" onclick="ubahnilai('.$key.')"></i>'):'';
                            $tombol2=($value2!='TOTAL' && !$datalocked)?'cursor:pointer" onclick="ubahketerangan('.$key.')':'';
                            $tombol3=($value2!='TOTAL' && !$datalocked)?'<i class="fa fa-trash" style="cursor:pointer" onclick="hapusdata('.$key.')"></i>':'';
                            if ($key2=='nama_perkiraan') echo '<td>'.$tombol.' '.$tombol3.'</td><td data-ket="'.$value2.'" style="background:white;'.$tombol2.'"> '.$value2.'</td>'; else {
                              echo '<td style="text-align:right" id="td'.$key.$key2.'" data-nilai="'.$value2.'">'.number_format($value2,2,',','.').'</td>';
                              $total+=$value2;
                              $totalsemua[$key2]+=$value2;
                            }
                          }
                        }
                      echo '<td style="display:none">'.number_format($total,2,',','.').'</td></tr>';
                    } ?>
                    <?php
                    $total=0;
                      echo '<tr id="trtotal" class="total" data-iddoc="0">';
                      echo '<td></td>';
                        foreach ($totalsemua as $key2 => $value2) {
                          if ($key2!='doc_id') {
                            if ($key2=='ket') echo '<td>TOTAL</td>'; else {
                              echo '<td style="text-align:right" id="td0total" data-nilai="'.$value2.'">'.number_format($value2,2,',','.').'</td>';
                              $total+=$value2;
                            }
                          }
                        }
                      echo '<td style="display:none">'.number_format($total,2,',','.').'</td></tr>';
                       ?>
                  </tbody>
              </table>
			  <?php } else { ?>
			  <p style="text-align:center">Data Tidak Ditemukan</p>
			  <?php } ?>
          </div>
        </div>
        <?php if ($session['hakakses']=='admin') { ?>
        <div class="arsip col-md-12" style="display:none">
          <div class="col-xs-6 col-md-4" style="padding: 6px;">
            <button type="button" onclick="tampilarsip(false)" class="btn btn-primary" style="cursor:pointer"> Kembali</button>
          </div>
          <div class="card-body table-full-width table-responsive" style="padding-top:0">
            <table id="tabledata" class="tableedit table table-hover table-striped table-bordered table-freeze" style="display:block;overflow-x:auto;">
              <thead><tr><th>Keterangan</th></tr></thead>
              <tbody>
                <?php foreach ($perkiraanarsip as $key => $value) {
                  echo '<tr id="trarsip'.$key.'" data-idper="'.$value['id_perkiraan'].'"><td data-ket="'.$value['nama_perkiraan'].'">'.$value['nama_perkiraan'].' <i class="fa fa-undo" style="cursor:pointer" onclick="archive('.$key.')"></i></td></tr>';
                } ?>
              </tbody>
            </table>
          </div>
        </div>
      <?php } ?>
    </div>
</div>

<div class="modal fade modal-primary" id="ubahnilai" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="-webkit-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);">
        <div class="modal-content">
          <form action="<?=base_url()?>data/<?=$table?>/simpan" method="post">
            <div class="modal-header justify-content-center">
                Ubah Nilai
            </div>
            <div class="modal-body">
              <input type="hidden" name="aksi" value="ubahnilai">
              <input type="hidden" name="tahun" value="<?=$tahun?>">
              <input type="hidden" name="unit" value="<?=$unit?>">
              <input type="hidden" name="id_perkiraan" class="id_perkiraan" value="">
              <table>
                <?php
                foreach (BULAN as $key => $bln) {
                  echo '<tr><td><label>'.BULAN_FULL[$key].'</label></td><td><input id="ubahnilai'.$bln.'" type="number" step="0.00000000000000001" class="form-control ubahnilaibulan" name="'.$bln.'" value="" placeholder="0" onfocus="ceknilai(this)" required /></td></tr>';
                }
                ?>
              </table>
			  <p class="totalsemuabulan"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" style="cursor:pointer">Simpan</button>
            </div>
          </form>
        </div>
    </div>
</div>


<div class="modal fade modal-primary" id="ubahketerangan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="-webkit-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);">
        <div class="modal-content">
          <form action="<?=base_url()?>data/<?=$table?>/simpanket" method="post">
            <div class="modal-header justify-content-center">
                Ubah Keterangan
            </div>
            <div class="modal-body">
              <input type="hidden" name="aksi" value="ubahketerangan">
              <input type="hidden" name="tahun" value="<?=$tahun?>">
              <input type="hidden" name="unit" value="<?=$unit?>">
              <input type="hidden" name="id_perkiraan" class="id_perkiraan" value="">
              <div class="col-xs-12">
                <label>Keterangan</label>
                <input type="text" class="form-control keterangan" name="nama_perkiraan" value="" >
              </div>
              <div class="col-xs-12 inputkelompok">
                <label>Kelompok</label>
                <select class="form-control" name="id_kelompok">
                  <?php foreach ($kelompokall as $key => $value) {
                    $selected=($table_id==$value['doc_id'])?' selected':'';
                    echo '<option value="'.$value['doc_id'].'"'.$selected.'>'.$value['kelompok'].'</option>';
                  } ?>
                </select>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" style="cursor:pointer">Simpan</button>
            </div>
          </form>
        </div>
    </div>
</div>

<div class="modal fade modal-primary" id="hapusdata" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="-webkit-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);">
        <div class="modal-content">
          <form action="<?=base_url()?>data/<?=$table?>/status" method="post">
            <div class="modal-header justify-content-center">
                Apakah Anda Yakin ingin Mengarsipkan Keterangan ini dan semua datanya ?
            </div>
            <div class="modal-body">
              <input type="hidden" name="aksi" value="ubahketerangan">
              <input type="hidden" name="tahun" value="<?=$tahun?>">
              <input type="hidden" name="unit" value="<?=$unit?>">
              <input type="hidden" name="id_perkiraan" class="id_perkiraan" value="">
              <input type="hidden" name="status" class="status" value="0">
              <h3></h3>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" style="cursor:pointer">Iya</button>
            </div>
          </form>
        </div>
    </div>
</div>

<div class="modal fade modal-primary" id="archive" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="-webkit-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);">
        <div class="modal-content">
          <form action="<?=base_url()?>data/<?=$table?>/status" method="post">
            <div class="modal-header justify-content-center">
                Apakah Anda Yakin ingin Mengembalikan Keterangan ini dan semua datanya ?
            </div>
            <div class="modal-body">
              <input type="hidden" name="aksi" value="ubahketerangan">
              <input type="hidden" name="tahun" value="<?=$tahun?>">
              <input type="hidden" name="unit" value="<?=$unit?>">
              <input type="hidden" name="id_perkiraan" class="id_perkiraan" value="">
              <input type="hidden" name="status" class="status" value="1">
              <h3></h3>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" style="cursor:pointer">Iya</button>
            </div>
          </form>
        </div>
    </div>
</div>

<script type="text/javascript">
  function ubahnilai(key=0) {
    $('#ubahnilai').modal();
    $('#ubahnilai .modal-header').html($('#tr'+key+' td:nth-child(2)').attr('data-ket'));
    $('#ubahnilai .id_perkiraan').val($('#tr'+key).attr('data-idper'));
    <?php foreach (BULAN as $key => $bln) {
      echo "$('#ubahnilai".$bln."').val($('#td'+key+'".$bln."').attr('data-nilai'));";
    } ?>
	jumlahsemuabulan();
  }
  function ubahketerangan(key=0) {
    $('#ubahketerangan').modal();
    $('#ubahketerangan .id_perkiraan').val($('#tr'+key).attr('data-idper'));
    $('#ubahketerangan .modal-header').html('Ubah Keterangan');
    $('#ubahketerangan .keterangan').val($('#tr'+key+' td:nth-child(2)').attr('data-ket'));
    $('#ubahketerangan .inputkelompok').show();
  }
  function tambahketerangan() {
    $('#ubahketerangan').modal();
    $('#ubahketerangan .id_perkiraan').val(0);
    $('#ubahketerangan .modal-header').html('Tambah Keterangan');
    $('#ubahketerangan .keterangan').val('');
    $('#ubahketerangan .inputkelompok').hide();
  }
  function hapusdata(key=0) {
    $('#hapusdata').modal();
    $('#hapusdata .id_perkiraan').val($('#tr'+key).attr('data-idper'));
    $('#hapusdata .modal-body h3').html($('#tr'+key+' td:nth-child(2)').attr('data-ket'));
  }
  function archive(key=0) {
    $('#archive').modal();
    $('#archive .id_perkiraan').val($('#trarsip'+key).attr('data-idper'));
    $('#archive .modal-body h3').html($('#trarsip'+key+' td:first-child').attr('data-ket'));
  }
  function tampilarsip(benar) {
    if (benar) {$('.formutama').hide();$('.arsip').show();} else {$('.arsip').hide();$('.formutama').show();}
  }
  function ceknilai(elemen) {
    if ($(elemen).val()==0) $(elemen).val('');
	  jumlahsemuabulan();
  }
  function jumlahsemuabulan(){
	var sum = 0;
	$('.ubahnilaibulan').each(function(){
		var val = (this.value!='')?this.value:0;
		sum += parseFloat(val);
	});
	$('.totalsemuabulan').html('Jumlah Semua : '+sum);
  }
  function fixedtable() {
    for (var i = 2; i <=2 ; i++) {
      $('.table-freeze tr td:nth-child('+i+'),.table-freeze tr th:nth-child('+i+')').css('left',parseInt($('.table-freeze tr td:nth-child('+(i-1)+')').css('left'))+parseInt($('.table-freeze tr td:nth-child('+(i-1)+')').outerWidth())+'px');
      $('.table-freeze tr td:nth-child('+i+')').css('z-index','9');
    }
  }
  window.addEventListener('DOMContentLoaded', (event) => {
    fixedtable();
    $('.judulhalaman').html('Budget <?=ucwords(str_replace('_',' ',str_replace('_dan_',' & ',$table)))?>');

  });
  <?php if ($session['hakakses']=='admin') { ?>
  function lockdata(lock) {
    window.location.href="<?=base_url().'data/'.$table.'/lockdata/?unit='.$unit.'&tahun='.$tahun.'&lock='?>"+lock;
  }
  <?php } ?>
</script>
