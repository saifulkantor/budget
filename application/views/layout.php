<?php
$session = $this->session->userdata();
$pesansession = $this->session->flashdata();
$path=explode("/", uri_string());
$menudipilih=isset($path[0])?$path[0]:'';
$submenudipilih=isset($path[1])?$path[1]:'';
$kelompokall = $this->Kelompok_Model->getAll()['data'];
$data=$data!=null?$data:[];
$data['table_id']=0;
if ($session['hakakses']=='user') {
  $_GET['unit']=$session['id_unit'];
}
$_GET['unit']=$_GET['unit']??1;
$_GET['tahun']=$_GET['tahun']??date('Y');
?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>template/creative-team/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?=base_url()?>template/creative-team/assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Proyeksi Apps <?=ucfirst($menudipilih)?></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="<?=base_url()?>assets/css/fonts/montserrat.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/fonts/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="<?=base_url()?>template/creative-team/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=base_url()?>template/creative-team/assets/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?=base_url()?>template/creative-team/assets/css/demo.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/css/main.css" rel="stylesheet" />
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-image="<?=base_url()?>template/creative-team/assets/img/sidebar-5.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="<?=base_url()?>" class="simple-text">
                        Budget Apps
                    </a>
                </div>
                <ul class="nav">
                    <li class="nav-item<?=($menudipilih=='')?' active':''?>">
                        <a class="nav-link" href="<?=base_url()?>">
                            <i class="nc-icon nc-chart-pie-35"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <?php if ($session['hakakses']=='admin') { ?>
                    <li class="<?=($menudipilih=='rekap')?' active':''?>">
                        <a class="nav-link" href="<?=base_url()?>rekap<?='?tahun='.$_GET['tahun']?>">
                            <i class="nc-icon nc-paper-2"></i>
                            <p>Rekap Data</p>
                        </a>
                    </li>
                  <?php } ?>
                    <li class="<?=($menudipilih=='laporan')?' active':''?>">
                        <a class="nav-link" href="<?=base_url()?>laporan<?='?unit='.$_GET['unit'].'&tahun='.$_GET['tahun']?>">
                            <i class="nc-icon nc-paper-2"></i>
                            <p>Laporan</p>
                        </a>
                    </li>
                      <?php foreach ($kelompokall as $key => $value) {
                        if ($value['kelompok']!='Bank' && $value['kelompok']!='Pihak 3'){
                          $link=str_replace(' ','_',strtolower($value['kelompok']));
                          $active=($menudipilih=='data'&&$submenudipilih==$link)?' active':'';
                          $data['table_id']=($active!='')?$value['doc_id']:$data['table_id'];
                          echo '<li class="'.$active.'">
                              <a class="nav-link" href="'.base_url().'data/'.$link.'?unit='.$_GET['unit'].'&tahun='.$_GET['tahun'].'">
                                  <i class="nc-icon nc-money-coins"></i>
                                  <p>'.str_replace(' dan ',' & ',$value['kelompok']).'</p>
                              </a>
                          </li>';
                        }
                      } ?>
                      <li><hr style="border-color:white"></li>
                      <li class="<?=($menudipilih=='bank'&&$submenudipilih=='pencairan')?' active':''?>">
                          <a class="nav-link" href="<?=base_url()?>bank/pencairan">
                              <i class="nc-icon nc-bank"></i>
                              <p>Pencairan Bank</p>
                          </a>
                      </li>
                      <li class="<?=($menudipilih=='bank'&&$submenudipilih=='pembayaran')?' active':''?>">
                          <a class="nav-link" href="<?=base_url()?>bank/pembayaran">
                              <i class="nc-icon nc-bank"></i>
                              <p>Pembayaran Bank</p>
                          </a>
                      </li>
                      <li><hr style="border-color:white"></li>
                      <?php if ($session['hakakses']=='admin') { ?>
                      <li class="nav-item">
                          <a class="nav-link" href="<?=base_url()?>user">
                            <i class="nc-icon nc-circle-09"></i>
                              <span class="no-icon">User</span>
                          </a>
                      </li>
                      <?php } ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=base_url()?>home/account">
                              <i class="nc-icon nc-circle-09"></i>
                                <span class="no-icon">Account</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?=base_url()?>auth/logout">
                              <i class="nc-icon nc-button-power"></i>
                                <span class="no-icon">Log out</span>
                            </a>
                        </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg " color-on-scroll="500">
                <div class="container-fluid">
                  <?php if (uri_string()!='' && uri_string()!='user' && uri_string()!='home/account') { ?>
                  <select class="tipehalaman" onchange="gantitipehalaman()">
                    <option value="<?=str_replace("realisasi/","data/",current_url()).'?'.$_SERVER['QUERY_STRING']?>" <?=((strpos(current_url(), 'realisasi') !== false)?'':'selected')?>>Budget</option>
                    <option value="<?=base_url().'realisasi/'.str_replace("data/","",uri_string()).'?'.$_SERVER['QUERY_STRING']?>" <?=((strpos(current_url(), 'realisasi') !== false)?'selected':'')?>>Realisasi</option>
                  </select>
                <?php } ?>
                  <h4 class="judulhalaman" style="margin: 0;"> Dashboard </h4>
                    <button href="" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <?php /*<div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <ul class="nav navbar-nav mr-auto">

                        </ul>
                        <ul class="navbar-nav ml-auto">
                          <?php if ($session['hakakses']=='admin') { ?>
                          <li class="nav-item">
                              <a class="nav-link" href="<?=base_url()?>user">
                                  <span class="no-icon">User</span>
                              </a>
                          </li>
                          <?php } ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?=base_url()?>home/account">
                                    <span class="no-icon">Account</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?=base_url()?>auth/logout">
                                    <span class="no-icon">Log out</span>
                                </a>
                            </li>
                        </ul>
                    </div> */ ?>
                </div>
            </nav>
            <!-- End Navbar -->
            <!-- <div class="content">
                <div class="container-fluid"> -->
                  <?php $this->load->view($halaman,$data); ?>
                <!-- </div>
            </div> -->
            <footer class="footer">
                <div class="container-fluid">
                    <nav>
                        <ul class="footer-menu">

                        </ul>
                        <p class="copyright text-center">
                            ©
                            <script>
                                document.write(new Date().getFullYear())
                            </script>
                            IT Team, made with love for a better web
                        </p>
                    </nav>
                </div>
            </footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="<?=base_url()?>template/creative-team/assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>template/creative-team/assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>template/creative-team/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="<?=base_url()?>template/creative-team/assets/js/plugins/bootstrap-switch.js"></script>
<!--  Chartist Plugin  -->
<script src="<?=base_url()?>template/creative-team/assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?=base_url()?>template/creative-team/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Light Bootstrap Dashboard: scripts for the example pages etc -->
<script src="<?=base_url()?>template/creative-team/assets/js/light-bootstrap-dashboard.js?v=2.0.0 " type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/Chart.min.js"></script>
<script src="<?=base_url()?>assets/js/utils.js"></script>
<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="<?=base_url()?>template/creative-team/assets/js/demo.js"></script>
<script src="<?=base_url()?>assets/js/main.js"></script>
<script type="text/javascript">
function gantitipehalaman() {
  location.href=$('.tipehalaman').val();
}
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        //demo.initDashboardPageCharts();
        lbd.initRightMenu();
        <?php if (isset($pesansession['errLoginMsg'])) {
          echo 'demo.showNotification("'.$pesansession['errLoginMsg'].'")';
        } ?>

    });
</script>

</html>
