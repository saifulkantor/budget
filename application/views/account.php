<?php $session = $this->session->userdata();
?>

<div>
    <div class="card strpied-tabled-with-hover">
        <?php /*<div class="card-header ">
            <h4 class="card-title">Akun Kamu</h4>
        </div> */ ?>
        <div class="card-body col-md-12">
          <ul class="nav nav-tabs" id="accounttab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="identitas-tab" data-toggle="tab" href="#identitas" role="tab" aria-controls="identitas" aria-selected="true">Identitas</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="akun-tab" data-toggle="tab" href="#akun" role="tab" aria-controls="akun" aria-selected="false">Akun</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="password-tab" data-toggle="tab" href="#password" role="tab" aria-controls="password" aria-selected="false">Password</a>
            </li>
          </ul>
          <div class="tab-content" id="accounttabContent" style="padding:20px 0px">
            <div class="tab-pane fade show active" id="identitas" role="tabpanel" aria-labelledby="identitas-tab">
              <form action="" method="post">
                <input type="hidden" name="id_user" value="<?=$session['id_user']?>" />
                <div class="col-xs-6 col-md-4" style="float:left">
                  <label>Nama</label>
                  <input type="text" class="form-control" name="nama" value="<?=$session['nama']?>" placeholder="Nama" required>
                </div>
                <div class="col-xs-6 col-md-4" style="float:left">
                  <label>Email</label>
                  <input type="email" class="form-control" name="email" value="<?=$session['email']?>" placeholder="Email" required >
                </div>
                <div class="col-xs-6 col-md-4" style="float:left">
                  <label>Password Sekarang</label>
                  <input type="password" class="form-control" name="password" value="" placeholder="*********" required >
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="col-xs-12" style="float:left">
                  <button type="submit" class="btn btn-primary" style="cursor:pointer">Simpan</button>
                </div>
              </form>
            </div>

            <div class="tab-pane fade" id="akun" role="tabpanel" aria-labelledby="akun-tab">
              <form action="" method="post">
                <input type="hidden" name="id_user" value="<?=$session['id_user']?>" />
                <div class="col-xs-6 col-md-4" style="float:left">
                  <label>Username</label>
                  <input type="text" class="form-control" name="username" value="<?=$session['username']?>" placeholder="Username" required >
                </div>
                <div class="col-xs-6 col-md-4" style="float:left">
                  <label>Password Sekarang</label>
                  <input type="password" class="form-control" name="password" value="" placeholder="*********" required >
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="col-xs-12" style="float:left">
                  <button type="submit" class="btn btn-primary" style="cursor:pointer">Simpan</button>
                </div>
              </form>
            </div>

            <div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="password-tab">
              <form action="" method="post">
                <input type="hidden" name="id_user" value="<?=$session['id_user']?>" />
                <div class="col-xs-6 col-md-4" style="float:left">
                  <label>Password Sekarang</label>
                  <input type="password" class="form-control" name="password" value="" placeholder="*********" required >
                </div>
                <div class="col-xs-6 col-md-4" style="float:left">
                  <label>Password Baru</label>
                  <input type="password" class="form-control" name="newpassword" value="" placeholder="*********" required >
                </div>
                <div class="col-xs-6 col-md-4" style="float:left">
                  <label>Ulangi Password Baru</label>
                  <input type="password" class="form-control" name="newpassword2" value="" placeholder="*********" required >
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="col-xs-12" style="float:left">
                  <button type="submit" class="btn btn-primary" style="cursor:pointer">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  window.addEventListener('DOMContentLoaded', (event) => {
    $('.judulhalaman').html('Akun Kamu');
  });
</script>
