<?php $session = $this->session->userdata();
$useraktif = $this->User_Model->getAll(1);
$usernonaktif = $this->User_Model->getAll(0);
$unitall = $this->Unit_Model->getAll()['data'];
?>

<div>
    <div class="card strpied-tabled-with-hover">
        <?php /*<div class="card-header ">
            <h4 class="card-title">User Aktif</h4>
        </div> */ ?>
        <div class="card-body col-md-12">
          <div class="listuseraktif">
            <div class="col-xs-12">
              <button type="button" onclick="tambahuser()" class="btn btn-primary btn-fill" style="cursor:pointer"><i class="nc-icon nc-simple-add"></i></button>
              <button type="button" onclick="tampilarsip(true)" class="btn btn-primary btn-fill" style="cursor:pointer"><i class="nc-icon nc-attach-87"></i></button>
            </div>
            <table id="tabledata" class="tableedit table table-hover table-striped table-bordered table-freeze" style="display:block;overflow-x:auto;width:min-content">
              <thead><tr><th></th><th>Nama User</th><th>Username</th><th>Email</th><th>Hak Akses</th></tr></thead>
              <tbody>
                <?php foreach ($useraktif as $key => $value) {
                  echo '<tr id="truser'.$value['id_user'].'" data-iduser="'.$value['id_user'].'"><td> <i class="nc-icon nc-ruler-pencil" style="cursor:pointer" onclick="ubahuser('.$key.')"></i> <i class="fa fa-trash" style="cursor:pointer" onclick="aktifkanuser('.$value['id_user'].',0)"></i> </td><td style="display: table-cell;">'.$value['nama'].'</td><td>'.$value['username'].'</td><td>'.$value['email'].'</td><td>'.$value['hakakses'].'</td></tr>';
                } ?>
              </tbody>
            </table>
          </div>
          <div class="listusertidakaktif" style="display:none">
            <div class="col-xs-12">
              <button type="button" onclick="tampilarsip(false)" class="btn btn-primary" style="cursor:pointer"> Kembali</button>
            </div>
            <table id="tabledata" class="tableedit table table-hover table-striped table-bordered table-freeze" style="display:block;overflow-x:auto;width:min-content">
              <thead><tr><th></th><th>Nama User</th><th>Username</th><th>Email</th><th>Hak Akses</th></tr></thead>
              <tbody>
                <?php foreach ($usernonaktif as $key => $value) {
                  echo '<tr id="truser'.$value['id_user'].'" data-iduser="'.$value['id_user'].'"><td> <i class="fa fa-undo" style="cursor:pointer" onclick="aktifkanuser('.$value['id_user'].',1)"></i></td><td style="display: table-cell;">'.$value['nama'].'</td><td>'.$value['username'].'</td><td>'.$value['email'].'</td><td>'.$value['hakakses'].'</td></tr>';
                } ?>
              </tbody>
            </table>
          </div>
        </div>
    </div>
</div>

<div class="edituser modal fade modal-primary" id="edituser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" style="-webkit-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);">
      <div class="modal-content">
      <form action="" method="post">
        <div class="modal-header justify-content-center">
            User
        </div>
        <div class="modal-body">
          <input type="hidden" class="id_user" name="id_user" value="0" />
          <input type="hidden" class="aktif" name="aktif" value="1" />
          <div class="col-xs-12" style="width: 100%;float:left">
            <label>Nama</label>
            <input type="text" class="form-control nama" name="nama" value="" placeholder="Nama" required>
          </div>
          <div class="col-xs-12" style="width: 100%;float:left">
            <label>Email</label>
            <input type="email" class="form-control email" name="email" value="" placeholder="Email" required >
          </div>
          <div class="col-xs-12" style="width: 100%;float:left">
            <label>Username</label>
            <input type="text" class="form-control username" name="username" value="" placeholder="Username" required >
          </div>
          <div class="col-xs-12" style="width: 100%;float:left">
            <label>Password Baru</label>
            <input type="password" class="form-control newpassword" name="newpassword" value="" placeholder="*********" >
          </div>
          <div class="col-xs-12" style="width: 100%;float:left">
            <label>Hak Akses</label>
            <select class="form-control hakakses" name="hakakses" required onchange="gantihakakses()">
              <option value="admin">Admin</option>
              <option value="user">User</option>
            </select>
          </div>
          <div class="col-xs-12 editunit" style="width: 100%;float:left">
            <label>Unit</label>
            <select class="form-control id_unit" name="id_unit">
              <?php foreach ($unitall as $key => $value) {
                echo '<option value="'.$value['doc_id'].'">'.$value['unit'].'</option>';
              } ?>
            </select>
          </div>
          <div class="clearfix">&nbsp;</div>
        </div>
        <div class="modal-footer">
          <div class="col-xs-12" style="float:left">
            <button type="submit" class="btn btn-primary" style="cursor:pointer">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade modal-primary" id="aktifkan0" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="-webkit-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);">
        <div class="modal-content">
          <form action="<?=base_url()?>home/useraktif" method="post">
            <div class="modal-header justify-content-center">
                Apakah Anda Yakin ingin Menonaktifkan User ini ?
            </div>
            <div class="modal-body">
              <input type="hidden" name="id_user" class="id_user" value="">
              <input type="hidden" name="aktif" class="aktif" value="0">
              <h3></h3>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" style="cursor:pointer">Iya</button>
            </div>
          </form>
        </div>
    </div>
</div>

<div class="modal fade modal-primary" id="aktifkan1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="-webkit-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);">
        <div class="modal-content">
          <form action="<?=base_url()?>home/useraktif" method="post">
            <div class="modal-header justify-content-center">
                Apakah Anda Yakin ingin Mengaktifkan User ini ?
            </div>
            <div class="modal-body">
              <input type="hidden" name="id_user" class="id_user" value="">
              <input type="hidden" name="aktif" class="aktif" value="1">
              <h3></h3>
              <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" style="cursor:pointer">Iya</button>
            </div>
          </form>
        </div>
    </div>
</div>

<script type="text/javascript">
var datauser=<?=json_encode($useraktif)?>;
function tambahuser() {
    $('#edituser input').val('');
    $('#edituser input.id_user').val(0);
    $('#edituser input.aktif').val(1);
    $('#edituser .hakakses').val('user').trigger('change');
    $('#edituser .id_unit').val(1);
    $('#edituser .modal-header').html('Tambah User');
    $('#edituser').modal();
}
function ubahuser(key=0) {
    $('#edituser input').val('');
    $('#edituser input.aktif').val(1);
    $.each(datauser[key],function(index,value){
      if (index=='id_unit' && value==null) value=1;
      $('#edituser .'+index).val(value).trigger('change');
    });
    $('#edituser .modal-header').html('Ubah User');
    $('#edituser').modal();
}
function tampilarsip(benar) {
  if (benar) {
    $('.judulhalaman').html('User Tidak Aktif');
    $('.listuseraktif').hide();$('.listusertidakaktif').show();
  } else {
    $('.judulhalaman').html('User Aktif');
    $('.listusertidakaktif').hide();$('.listuseraktif').show();
  }
}
function tampiluser(key=0) {
  $('#edituser').modal();
}
function aktifkanuser(id_user=0,aktif=1) {
  $('#aktifkan'+aktif+' .id_user').val(id_user);
  $('#aktifkan'+aktif).modal();
}
function gantihakakses(){
  if ($('.hakakses').val()=='admin') {
    $('.editunit').hide();
  } else {
    $('.editunit').show();
  }
}
window.addEventListener('DOMContentLoaded', (event) => {
  $('.judulhalaman').html('User Aktif');
});
</script>
