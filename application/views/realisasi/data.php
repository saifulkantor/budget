<?php
// $idkelompok = array(
//   'revenue' => 1,
//   'fixed_cost' => 2,
//   'variable_cost' => 3,
//   'fixed_cost_tahunan' => 4,
//   'bank_dan_kendaraan' => 5,
//   'capex' => 6,
//   'csr' => 7,
//   'Bank' => 8,
//   'Pihak3' => 9,
//   'project' => 1009
// );
$session = $this->session->userdata();
if ($session['hakakses']=='user') {
  $_GET['unit']=$session['id_unit'];
}
$tahun = isset($_GET['tahun'])?$_GET['tahun']:date('Y');
$bulan = isset($_GET['bulan'])?$_GET['bulan']:date('m');
$bulan = ($bulan>=1 && $bulan<=12)?$bulan:1;
$unit = isset($_GET['unit'])?$_GET['unit']:1;
$tanggalmulai=date("Y-m-d",strtotime($tahun."-".$bulan."-01"));
$tanggalakhir=date("Y-m-t",strtotime($tahun."-".$bulan."-01"));
$namaunit='';
$data = $this->sp2_model->exec($table,['tanggalmulai'=>$tanggalmulai,'tanggalakhir'=>$tanggalakhir,'unit'=>$unit])['rows'];
// $datalocked = $this->sp_model->exec('checklock',['tahun'=>$tahun,'unit'=>$unit,'id_kelompok'=>$idkelompok[$table]])['rows'][0]->locked;
$datalocked = $this->sp_model->exec('checklock',['tahun'=>$tahun,'unit'=>$unit])['rows'][0]->locked;
$datalocked =1;
$unitall = $this->Unit_Model->getAll()['data'];
$perkiraanarsip = $this->Perkiraan_Model->getArsip($table_id,$unit)['data'];
$kelompokall = $this->Kelompok_Model->getAll()['data'];
$totalsemua['ket']='TOTAL';
$updatejs='';
?>
<div>
    <div class="card strpied-tabled-with-hover">
        <?php /*<div class="card-header ">
            <h4 class="card-title">Realisasi <?=ucwords(str_replace('_',' ',str_replace('_dan_',' & ',$table)))?></h4>
        </div> */ ?>
        <div class="formutama col-md-12">
          <form action="" method="get">
            <div class="col-xs-6 col-md-4" style="float:left">
              <!-- <label>Unit</label> -->
              <select class="form-control" name="unit" onchange="this.form.submit()">
                <?php foreach ($unitall as $key => $value) {
                  $selected=($unit==$value['doc_id'])?' selected':'';
                  $namaunit=($unit==$value['doc_id'])?$value['unit']:$namaunit;
                  echo '<option value="'.$value['doc_id'].'"'.$selected.'>'.$value['unit'].'</option>';
                } ?>
              </select>
            </div>
            <div class="col-xs-3 col-md-2" style="float:left">
              <!-- <label>Bulan</label> -->
              <input type="number" class="form-control" name="bulan" min="1" max="12" onchange="this.form.submit()" value="<?=$bulan?>" title="BULAN" >
            </div>
            <div class="col-xs-6 col-md-4" style="float:left">
              <!-- <label>Year</label> -->
              <input type="number" class="form-control" name="tahun" onchange="this.form.submit()" value="<?=$tahun?>" title="TAHUN" >
            </div>
            <!-- <div class="col-xs-3 col-md-2" style="float:left;text-align:right"> -->
            <?php /* if ($session['hakakses']=='admin') { ?>
                <label style="width:100%">&nbsp;</label>
                <button type="button" onclick="lockdata(<?=!$datalocked?>)" class="btn btn-primary btn-fill" style="cursor:pointer"><?=($datalocked)?'<i class="fa fa-unlock"></i> Unlock':'<i class="fa fa-lock"></i> Lock'?></button>
            <?php } */ ?>
            <!-- </div> -->
            <div class="col-xs-3 col-md-2" style="float:left;text-align:right">
              <!-- <label style="width:100%">&nbsp;</label> -->
              <button type="button" onclick="fnExcelReport('tabledata','<?=ucwords(str_replace('_',' ',str_replace('_dan_',' & ',$table)))?> <?=$namaunit?> (<?=$tahun?>)')" class="btn btn-primary" style="cursor:pointer"> Export Excel</button>
            </div>
          </form>
          <div class="card-body table-full-width table-responsive">
              <div class="col-xs-12">
                <?php // if ($session['hakakses']=='admin') {
                if (!$datalocked) { ?>
                <button type="button" onclick="tambahketerangan()" class="btn btn-primary btn-fill" style="cursor:pointer"><i class="nc-icon nc-simple-add"></i></button>
                <button type="button" onclick="tampilarsip(true)" class="btn btn-primary btn-fill" style="cursor:pointer"><i class="nc-icon nc-attach-87"></i></button>
                <?php } ?>
              </div>
			  <?php if (isset($data[0])) { ?>
          <form name="" action="<?=base_url()?>realisasi/<?=$table?>/simpan" method="post" >
              <input type="hidden" name="unit" value="<?=$unit?>" >
              <input type="hidden" name="bulan" value="<?=$bulan?>" >
              <input type="hidden" name="tahun" value="<?=$tahun?>" >

              <table id="tabledata" class="tableedit table table-hover table-striped table-bordered table-freeze" style="display:block;overflow-x:auto;">
                  <thead><tr>
                    <?php
					foreach ($data[0] as $key => $value) {
                      if ($key=='id_perkiraan') echo ''; else if ($key=='nama_perkiraan') echo '<th style="left:0px;z-index:10!important">Keterangan</th>';
                      else if ((int)$key>=1 && (int)$key<=12 ) {
                        echo '<th style="z-index: 10!important;">'.lengkapibulan(BULAN[$key-1]).'</th>';
                        echo '<th style="z-index: 10!important;">REALISASI</th><th style="z-index: 10!important;">DEV</th>';
                        $totalsemua[$key]=0;
                    } else {
                        echo '<th>'.date('d',strtotime($key)).'</th>';
                        $totalsemua[$key]=0;
                    }
                    }
                    // print_r($totalsemua);
                    ?>
                  <!-- <th style="display:none">TOTAL</th> -->
                  </tr>
                  </thead>
                  <tbody>
                    <?php
                    foreach ($data as $key => $value) {
                      echo '<tr'.(($value->nama_perkiraan=='TOTAL')?' class="total"':' class="trdata"').' title="'.$value->nama_perkiraan.'" id="tr'.$key.'" data-idper="'.$value->id_perkiraan.'" data-key="'.$key.'">';
                      $total=0;
                        foreach ($data[$key] as $key2 => $value2) {
                          if ($key2!='id_perkiraan') {
                            $tombol=($value2!='TOTAL')?(($datalocked)?'<i class="fa fa-lock"></i>':'<i class="nc-icon nc-ruler-pencil" style="cursor:pointer" onclick="ubahnilai('.$key.')"></i>'):'';
                            $tombol = '';
                            $tombol2=($value2!='TOTAL' && !$datalocked)?'<i class="fa fa-edit" style="cursor:pointer" onclick="ubahketerangan('.$key.')"></i>':'';
                            $tombol3=($value2!='TOTAL' && !$datalocked)?'<i class="fa fa-trash" style="cursor:pointer" onclick="hapusdata('.$key.')"></i>':'';
                            if ($key2=='nama_perkiraan') echo '<td class="datapentingtabel" data-ket="'.$value2.'" style="left:0px;z-index:9;background:white">'.$value2.' '.$tombol2.' '.$tombol3.'</td>';
                            else if ((int)$key2>=1 && (int)$key2<=12 ) {
                              echo '<td style="text-align:right;background:white;z-index: 9;" class="b'.$value->id_perkiraan.' datapentingtabel" id="td'.$value->id_perkiraan.$key2.'" data-nilai="'.$value2.'">'.number_format($value2,2,',','.').'</td>';
                              echo '<td class="r'.$value->id_perkiraan.' datapentingtabel" style="text-align:right;background:white;z-index: 9;"></td><td class="d'.$value->id_perkiraan.' datapentingtabel" style="text-align:right;background:white;z-index: 9;"></td>';
                              $total+=$value2;
                              $totalsemua[$key2]+=$value2;
                            } else {
                              echo '<td style="text-align:right;cursor:pointer" class="a'.$value->id_perkiraan.' tgl'.$key2.' '.$value->id_perkiraan.'h'.$key2.'" id="td'.$key.$key2.'" onclick="buatinput(\''.$value->id_perkiraan.'h'.$key2.'\')" data-nilai="'.((float)$value2).'" data-nilaiawal="'.((float)$value2).'" data-idper="'.$value->id_perkiraan.'">'.number_format($value2,2,',','.').'</td>';
                              $total+=$value2;
                              $totalsemua[$key2]+=$value2;
                            }
                          }
                        }
                      // echo '<td style="display:none">'.number_format($total,2,',','.').'</td>'
                      echo '</tr>';
                    } ?>
                    <?php
                    $total=0;
                      echo '<tr id="trtotal" class="total" data-iddoc="0">';
                      // echo '<td></td>';
                        foreach ($totalsemua as $key2 => $value2) {
                          if ($key2!='doc_id') {
                            if ($key2=='ket') echo '<td style="left:0px;z-index:9">TOTAL</td>';
                            else if ((int)$key2>=1 && (int)$key2<=12 ) {
                              echo '<td style="text-align:right;z-index:9" id="td0total" data-nilai="'.$value2.'">'.number_format($value2,2,',','.').'</td>';
                              echo '<td class="totalr" data-nilai="0" style="text-align:right;"></td><td class="totald" data-nilai="0" style="text-align:right;"></td>';
                              $total+=$value2;
                            } else {
                              echo '<td style="text-align:right;" class="totaltgl total'.$key2.'" data-tgl="'.$key2.'" data-nilai="'.$value2.'">'.number_format($value2,2,',','.').'</td>';
                              $total+=$value2;
                            }
                          }
                        }
                      // echo '<td style="display:none">'.number_format($total,2,',','.').'</td>'
                      echo '</tr>';
                       ?>
                  </tbody>
              </table>
              <button type="submit" class="btn btn-primary" style="cursor:pointer">Simpan</button>
            </form>
			  <?php } else { ?>
			  <p style="text-align:center">Data Tidak Ditemukan</p>
			  <?php } ?>
          </div>
        </div>
        <?php if ($session['hakakses']=='admin') { ?>
        <div class="arsip" style="display:none">
          <div class="card-body table-full-width table-responsive">
            <div class="col-xs-12">
              <button type="button" onclick="tampilarsip(false)" class="btn btn-primary" style="cursor:pointer"> Kembali</button>
            </div>
            <table id="tabledata" class="tableedit table table-hover table-striped table-bordered" style="display:block;overflow-x:auto;">
              <thead><tr><th>Keterangan</th></tr></thead>
              <tbody>
                <?php foreach ($perkiraanarsip as $key => $value) {
                  echo '<tr id="trarsip'.$key.'" data-idper="'.$value['id_perkiraan'].'"><td data-ket="'.$value['nama_perkiraan'].'">'.$value['nama_perkiraan'].' <i class="fa fa-undo" style="cursor:pointer" onclick="archive('.$key.')"></i></td></tr>';
                } ?>
              </tbody>
            </table>
          </div>
        </div>
      <?php } ?>
    </div>
</div>


<script type="text/javascript">
  function ubahnilai(key=0) {
    $('#ubahnilai').modal();
    $('#ubahnilai .modal-header').html($('#tr'+key+' td:nth-child(2)').attr('data-ket'));
    $('#ubahnilai .id_perkiraan').val($('#tr'+key).attr('data-idper'));
    <?php foreach (BULAN as $key => $bln) {
      echo "$('#ubahnilai".$bln."').val($('#td'+key+'".$bln."').attr('data-nilai'));";
    } ?>
	jumlahsemuabulan();
  }
  function ubahketerangan(key=0) {
    $('#ubahketerangan').modal();
    $('#ubahketerangan .id_perkiraan').val($('#tr'+key).attr('data-idper'));
    $('#ubahketerangan .modal-header').html('Ubah Keterangan');
    $('#ubahketerangan .keterangan').val($('#tr'+key+' td:nth-child(2)').attr('data-ket'));
    $('#ubahketerangan .inputkelompok').show();
  }
  function tambahketerangan() {
    $('#ubahketerangan').modal();
    $('#ubahketerangan .id_perkiraan').val(0);
    $('#ubahketerangan .modal-header').html('Tambah Keterangan');
    $('#ubahketerangan .keterangan').val('');
    $('#ubahketerangan .inputkelompok').hide();
  }
  function hapusdata(key=0) {
    $('#hapusdata').modal();
    $('#hapusdata .id_perkiraan').val($('#tr'+key).attr('data-idper'));
    $('#hapusdata .modal-body h3').html($('#tr'+key+' td:nth-child(2)').attr('data-ket'));
  }
  function archive(key=0) {
    $('#archive').modal();
    $('#archive .id_perkiraan').val($('#trarsip'+key).attr('data-idper'));
    $('#archive .modal-body h3').html($('#trarsip'+key+' td:first-child').attr('data-ket'));
  }
  function tampilarsip(benar) {
    if (benar) {$('.formutama').hide();$('.arsip').show();} else {$('.arsip').hide();$('.formutama').show();}
  }
  function ceknilai(elemen) {
    if ($(elemen).val()==0) $(elemen).val('');
	  jumlahsemuabulan();
  }
  function jumlahsemuabulan(){
	var sum = 0;
	$('.ubahnilaibulan').each(function(){
		var val = (this.value!='')?this.value:0;
		sum += parseFloat(val);
	});
	$('.totalsemuabulan').html('Jumlah Semua : '+sum);
  }
  <?php if ($session['hakakses']=='admin') { ?>
  function lockdata(lock) {
    window.location.href="<?=base_url().'data/'.$table.'/lockdata/?unit='.$unit.'&tahun='.$tahun.'&lock='?>"+lock;
  }
  <?php } ?>
  function buatinput(kelas='') {
    simpanangkasementara();
    var nilai = $('.'+kelas).attr('data-nilai');
    $('.'+kelas).html('<input type="number" style="max-width: 100px;" class="inputangka" data-kelas="'+kelas+'" name="inputangka" value="'+nilai+'">');
    $('.inputangka').focus();
    $('.inputangka').select();
    $(".inputangka").on('keydown', function (e) {
        if (e.keyCode === 13) {
          simpanangkasementara();
          e.preventDefault();
          return false;
        } else if (e.keyCode === 27) {
          $('.inputangka').val(nilai);
          simpanangkasementara();
          e.preventDefault();
          return false;
        }
    });
  }
  function simpanangkasementara() {
    $('.inputangka').val(parseFloat($('.inputangka').val())  || 0);
    var kelas = $('.inputangka').attr('data-kelas');
    var idper = $('.'+kelas).attr('data-idper');
    var nilaiawal = $('.'+kelas).attr('data-nilaiawal');
    var warna = 'black'; var htmlplus = '';
    var div = parseFloat($('.inputangka').val())-parseFloat($('.'+kelas).attr('data-nilai'));
    if (div>parseFloat($('.d'+idper).attr('data-nilai')) && '<?=$table?>'!='revenue') {
      $('.inputangka').val(parseFloat($('.'+kelas).attr('data-nilai')));
      alert('[GAGAL] Nilai Realisasi Melebihi Budget');
    }
    if ($('.inputangka').val()!=nilaiawal) {
      warna = '#9368e9';
      htmlplus = ' <input type="hidden" name="gantinilai['+kelas+']" value="'+$('.inputangka').val()+'">';
    }

    $('.'+kelas).attr('data-nilai',$('.inputangka').val());
    $('.'+kelas).css('color',warna);
    $('.'+kelas).html(parseFloat($('.inputangka').val()).toFixed(2).toString().replace(".",",").replace(/\B(?=(\d{3})+(?!\d))/g, ".")+''+htmlplus);
    hitungdata();
    fixedtable();
  }

  function hitungdata() {
    var totalr = 0;
    var totald = 0;
    $.each($('.trdata'),function(idx,val){
        var idper = $(val).attr('data-idper');
        var total = 0;
        $.each($('.a'+idper),function(idx2,val2){
            total+=parseFloat($(val2).attr('data-nilai'));
        });
        var budget = parseFloat($('.b'+idper).attr('data-nilai'));
        $('.r'+idper).html(parseFloat(total).toFixed(2).toString().replace(".",",").replace(/\B(?=(\d{3})+(?!\d))/g, "."));
        $('.r'+idper).attr('data-nilai',total)
        totalr+=parseFloat(total);
        $('.d'+idper).html(parseFloat(budget-total).toFixed(2).toString().replace(".",",").replace(/\B(?=(\d{3})+(?!\d))/g, "."));
        $('.d'+idper).attr('data-nilai',budget-total)
        totald+=parseFloat(budget-total);
    });
    $('.totalr').html(parseFloat(totalr).toFixed(2).toString().replace(".",",").replace(/\B(?=(\d{3})+(?!\d))/g, "."));
    $('.totalr').attr('data-nilai',totalr);
    $('.totald').html(parseFloat(totald).toFixed(2).toString().replace(".",",").replace(/\B(?=(\d{3})+(?!\d))/g, "."));
    $('.totald').attr('data-nilai',totald);
    $.each($('.totaltgl'),function(idx,val){
      var tgl = $(val).attr('data-tgl');
      var total=0;
      $.each($('.tgl'+tgl),function(idx2,val2){
          total+=parseFloat($(val2).attr('data-nilai'));
      });
      $('.total'+tgl).html(parseFloat(total).toFixed(2).toString().replace(".",",").replace(/\B(?=(\d{3})+(?!\d))/g, "."));
      $('.total'+tgl).attr('data-nilai',total);
    });

  }
  function fixedtable() {
    for (var i = 2; i <=4 ; i++) {
      $('.table-freeze tr td:nth-child('+i+'),.table-freeze tr th:nth-child('+i+')').css('left',parseInt($('.table-freeze tr td:nth-child('+(i-1)+')').css('left'))+parseInt($('.table-freeze tr td:nth-child('+(i-1)+')').outerWidth())+'px');
      $('.table-freeze tr td:nth-child('+i+')').css('z-index','9');
    }
  }

  window.addEventListener('DOMContentLoaded', (event) => {
    $('.judulhalaman').html('Realisasi <?=ucwords(str_replace('_',' ',str_replace('_dan_',' & ',$table)))?>');
    
    hitungdata();
    fixedtable();
  });
</script>
