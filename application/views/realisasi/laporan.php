<?php
$session = $this->session->userdata();
$tahun = isset($_GET['tahun'])?$_GET['tahun']:date('Y');
$unit = isset($_GET['unit'])?$_GET['unit']:1;
$namaunit='';
$data = $this->sp2_model->exec('laporan',['tahun'=>$tahun,'unit'=>$unit])['rows'];
$datalocked = $this->sp_model->exec('checklock',['tahun'=>$tahun,'unit'=>$unit])['rows'][0]->locked;
$unitall = $this->Unit_Model->getAll()['data'];
$kelompokall = $this->Kelompok_Model->getAll()['data'];
$totalsemua['ket']='TOTAL';
$totalperkeldefault['ket']='TOTAL';
$totalpengeluaran['ket']='TOTAL';
$totalsisa['ket']='TOTAL';
?>
<div>
    <div class="card strpied-tabled-with-hover">
        <?php /* <div class="card-header ">
            <h4 class="card-title">Laporan Realisasi <?=($datalocked)?'(<i class="fa fa-lock"></i>)':''?></h4>
        </div> */ ?>
        <div class="formutama col-md-12" >
          <form action="" method="get">
            <div class="col-xs-6 col-md-4" style="float:left">
              <!-- <label>Unit</label> -->
              <select class="form-control listunit" name="unit" onchange="this.form.submit()">
                <?php foreach ($unitall as $key => $value) {
                  $selected=($unit==$value['doc_id'])?' selected':'';
                  $namaunit=($unit==$value['doc_id'])?$value['unit']:$namaunit;
                  echo '<option value="'.$value['doc_id'].'"'.$selected.'>'.$value['unit'].'</option>';
                } ?>
              </select>
            </div>
            <div class="col-xs-6 col-md-4" style="float:left">
              <!-- <label>Year</label> -->
              <input type="number" class="form-control" name="tahun" onchange="this.form.submit()" value="<?=$tahun?>" >
            </div>
            <div class="col-xs-3 col-md-2" style="float:left">
            <?php if ($session['hakakses']=='admin') { ?>
                <label style="width:100%">&nbsp;</label>
                <?php /* <button type="button" onclick="lockdata(<?=!$datalocked?>)" class="btn btn-primary btn-fill" style="cursor:pointer"><?=($datalocked)?'<i class="fa fa-unlock"></i> Unlock':'<i class="fa fa-lock"></i> Lock'?></button>
                */ ?>
            <?php } ?>
            </div>
            <div class="col-xs-3 col-md-2" style="float:right;text-align:right">
              <!-- <label style="width:100%">&nbsp;</label> -->
              <button type="button" onclick="fnExcelReport('tablerekap','Laporan <?=$namaunit?> (<?=$tahun?>)')" class="btn btn-primary" style="cursor:pointer"> Export Excel</button>
            </div>
          </form>
          <div class="card-body table-full-width table-responsive">
			  <?php if (isset($data[0])) { ?>
              <table id="tablerekap" class="table table-hover table-striped table-bordered table-freeze" style="display:block;overflow-x:auto;">
                  <thead><tr>
                    <?php foreach ($data[0] as $key => $value) {
                      if ($key=='doc_id') echo ''; else if ($key=='kelompok') echo ''; else if ($key=='nama_perkiraan') echo '<th>Keterangan</th><th style="display:none">TOTAL</th>'; else {
                        echo '<th>'.lengkapibulan($key).'</th>';
                        $totalsemua[$key]=0; $totalperkeldefault[$key]=0; $totalpengeluaran[$key]=0; $totalsisa[$key]=0;
                      }
                    }
                    $totalperkelompok=$totalperkeldefault;
                    ?>
                  </tr>
                  </thead>
                  <tbody>
                    <?php $projectdata = array(); $idlama=0;
                    foreach ($data as $key => $value) {
                      if ($value->doc_id==1009) {
                        $projectdata[] = $data[$key];
                      } else {
                        if ($value->doc_id!=$idlama && $value->doc_id!=1) {
                          echo '<tr class="total" data-idper="">';
                          $echotable='';
                          $total=0;
                          foreach ($totalperkelompok as $keyt => $totalarr) {
                            if ($keyt=='ket') {
                              $teks=($idlama==1)?'TOTAL REVENUE':$totalarr;
                              echo '<td data-ket="'.$totalarr.'">'.$teks.'</td>';
                            } else {
                              /*echo*/ $echotable.='<td style="text-align:right" id="tdtot'.$keyt.'" data-nilai="'.$totalarr.'">'.number_format($totalarr,2,',','.').'</td>';
                              $total+=$totalarr;
                              if ($idlama==2 || $idlama==3 || $idlama==4) $totalpengeluaran[$keyt]+=$totalarr; else if ($idlama!=1) $totalsisa[$keyt]+=$totalarr;
                            }
                          }
                          echo '<td style="display:none">'.number_format($total,2,',','.').'</td>'.$echotable.'</tr>';
                          if ($idlama==4) {
                            echo '<tr class="totalpengeluaran" data-idper="">';
                            $echotable='';
                            $total=0;
                            foreach ($totalpengeluaran as $keyt => $totalarr) {
                              if ($keyt=='ket') {
                                echo '<td data-ket="'.$totalarr.'">TOTAL PENGELUARAN</td>';
                              } else {
                                /*echo*/ $echotable.='<td style="text-align:right" id="tdtot'.$keyt.'" data-nilai="'.$totalarr.'">'.number_format($totalarr,2,',','.').'</td>';
                                $total+=$totalarr;
                              }
                            }
                            echo '<td style="display:none">'.number_format($total,2,',','.').'</td>'.$echotable.'</tr>';
                          }
                        }
                        if ($value->doc_id!='1'&& $idlama==1 && count($projectdata)>0) {
                          echo '<tr class="total" data-idper="" onclick="kehalaman(\'project\')" style="cursor:pointer">
                          <td data-ket="">Project</td>
                          <td colspan="13"></td></tr>';
                          $totalperkelompok=$totalperkeldefault;
                          foreach ($projectdata as $keyp => $project) {
                            echo '<tr'.(($project->kelompok=='TOTAL')?' class="total"':'').' id="tr'.$keyp.'" data-idper="'.$project->doc_id.'">';
                            $echotable='';
                            $total=0;
                            foreach ($project as $key2 => $value2) {
                              if ($key2!='doc_id') {
                                if ($key2=='nama_perkiraan') echo '<td data-ket="'.$value2.'">'.$value2.'</td>'; else if ($key2=='kelompok'){
                                  // echo '<td data-ket="'.$value2.'">'.$value2.'</td>';
                                } else {
                                  /*echo*/ $echotable.='<td style="text-align:right" id="td'.$keyp.$key2.'" data-nilai="'.$value2.'">'.number_format($value2,2,',','.').'</td>';
                                  $total+=$value2;
                                  if ($project->doc_id==1) $totalsemua[$key2]+=$value2; else $totalsemua[$key2]-=$value2;
                                  $totalperkelompok[$key2]+=$value2;
                                  if ($idlama==2 || $idlama==3 || $idlama==4) $totalpengeluaran[$keyt]+=$totalarr; else if ($idlama!=1) $totalsisa[$keyt]+=$totalarr;
                                }
                              }
                            }
                            echo '<td style="display:none">'.number_format($total,2,',','.').'</td>'.$echotable.'</tr>';
                          }
                          echo '<tr class="total" data-idper="">';
                          $echotable='';
                          $total=0;
                          foreach ($totalperkelompok as $keyt => $totalarr) {
                            if ($keyt=='ket'){
                              echo '<td data-ket="'.$totalarr.'">'.$totalarr.'</td>';
                            } else {
                              /*echo*/ $echotable.='<td style="text-align:right" id="tdtot'.$keyt.'" data-nilai="'.$totalarr.'">'.number_format($totalarr,2,',','.').'</td>';
                              $total+=$totalarr;
                              $totalpengeluaran[$keyt]+=$totalarr;
                            }
                          }
                          echo '<td style="display:none">'.number_format($total,2,',','.').'</td>'.$echotable.'</tr>';
                        }
                        if ($value->doc_id!=$idlama) {
                          $totalperkelompok=$totalperkeldefault;
                          $tambahan='';
                          if ($value->kelompok!='Bank' && $value->kelompok!='Pihak 3'){
                            $link=str_replace(' ','_',strtolower($value->kelompok));
                            $tambahan=' onclick="kehalaman(\''.$link.'\')" style="cursor:pointer"';
                          }
                          echo '<tr class="total" id="tr'.$key.'" data-idper="'.$value->doc_id.'"'.$tambahan.'>
                          <td data-ket="">'.$value->kelompok.'</td>
                          <td colspan="13"></td></tr>';
                        }
                        echo '<tr'.(($value->kelompok=='TOTAL')?' class="total"':'').' id="tr'.$key.'" data-idper="'.$value->doc_id.'">';
                        $echotable='';
                        $total=0;
                        foreach ($data[$key] as $key2 => $value2) {
                          if ($key2!='doc_id') {
                            if ($key2=='nama_perkiraan') echo '<td data-ket="'.$value2.'">'.$value2.'</td>'; else if ($key2=='kelompok'){
                              // echo '<td data-ket="'.$value2.'">'.$value2.'</td>';
                            }  else {
                              $link=str_replace(' ','_',strtolower($value->kelompok));
                              $idx = array_search(ucfirst($key2),BULAN);
                              $tambahan=' onclick="kerealisasi(\''.$link.'\','.($idx+1).')" ';
                              /*echo*/ $echotable.='<td style="text-align:right;cursor:pointer" id="td'.$key.$key2.'" data-nilai="'.$value2.'" '.$tambahan.'>'.number_format($value2,2,',','.').'</td>';
                              $total+=$value2;
                              if ($value->doc_id==1) $totalsemua[$key2]+=$value2; else $totalsemua[$key2]-=$value2;
                              $totalperkelompok[$key2]+=$value2;
                            }
                          }
                        }
                        echo '<td style="display:none">'.number_format($total,2,',','.').'</td>'.$echotable.'</tr>';
                        $idlama=$value->doc_id;
                      }
                    } ?>
                    <?php
                    echo '<tr class="total" data-idper="">';
                    $echotable='';
                    $total=0;
                    foreach ($totalperkelompok as $keyt => $totalarr) {
                      if ($keyt=='ket') {
                        echo '<td data-ket="'.$totalarr.'">'.$totalarr.'</td>';
                      } else {
                        /*echo*/ $echotable.='<td style="text-align:right" id="tdtot'.$keyt.'" data-nilai="'.$totalarr.'">'.number_format($totalarr,2,',','.').'</td>';
                        $total+=$totalarr;
                        if ($idlama==2 || $idlama==3 || $idlama==4) $totalpengeluaran[$keyt]+=$totalarr; else if ($idlama!=1) $totalsisa[$keyt]+=$totalarr;
                      }
                    }
                    echo '<td style="display:none">'.number_format($total,2,',','.').'</td>'.$echotable.'</tr>';

                    $total=0;
                      echo '<tr id="trtotal" class="totalsisa" data-iddoc="0">';
                      $echotable='';
                        foreach ($totalsemua as $key2 => $value2) {
                          if ($key2!='doc_id') {
                            if ($key2=='ket') echo '<td>SISA</td>'; else {
                              /*echo*/ $echotable.='<td style="text-align:right" id="td0total" data-nilai="'.$value2.'">'.number_format($value2,2,',','.').'</td>';
                              $total+=$value2;
                            }
                          }
                        }
                      echo '<td style="display:none">'.number_format($total,2,',','.').'</td>'.$echotable.'</tr>';
                       ?>

                  </tbody>
              </table>
			  <?php } else { ?>
			  <p style="text-align:center">Data Tidak Ditemukan</p>
			  <?php } ?>
          </div>
        </div>
    </div>
</div>
<script type="text/javascript">
<?php if ($session['hakakses']=='admin') { ?>
  function lockdata(lock=0) {
    var kata = (lock==1)?'Mengunci':'Membuka';
    if (confirm('Apakah Anda yakin ingin '+kata+' Data '+$('.listunit option:selected').text()+' ?')) {
      window.location.href="<?=base_url().'laporan/lockdata/?unit='.$unit.'&tahun='.$tahun.'&lock='?>"+lock;
    }
  }
<?php } ?>
function kehalaman(perkiraan='') {
  location.href='<?=base_url()?>data/'+perkiraan+'?unit=<?=$unit?>&tahun=<?=$tahun?>';
}
function kerealisasi(perkiraan='',bulan=1) {
  location.href='<?=base_url()?>realisasi/'+perkiraan+'?unit=<?=$unit?>&bulan='+bulan+'&tahun=<?=$tahun?>';
}
window.addEventListener('DOMContentLoaded', (event) => {
  $('.judulhalaman').html('Laporan Realisasi <?=($datalocked)?'(<i class="fa fa-lock"></i>)':''?>');

});

</script>
