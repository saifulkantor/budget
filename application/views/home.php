<?php
$session = $this->session->userdata();
$tahun = isset($_GET['tahun'])?$_GET['tahun']:date('Y');
$unit = isset($_GET['unit'])?$_GET['unit']:1;

$datatotal = $this->sp_model->exec('rekaptotal',['tahun'=>$tahun])['rows'];
$datakredit = $this->sp_model->exec('kredit',['tahun'=>$tahun])['rows'];
$datapemhutang = $this->sp_model->exec('hp3',['tahun'=>$tahun])['rows'];
$unitall = $this->Unit_Model->getAll()['data'];
$kelompokall = $this->Kelompok_Model->getAll()['data'];
?>
<div class="col-xs-12">
    <div class="card ">
        <?php /*<div class="card-header ">
          <h4 class="card-title">Aplikasi Budget</h4>
        </div> */ ?>
        <div class="card-body all-icons">
          <?php if ($session['hakakses']=='admin') { ?>
          <div class="row">
            <div class="font-icon-list col-xs-6 col-sm-4 col-md-3">
              <div class="font-icon-detail" style="min-height: 10px;padding: 30px 12px;background-color: #282eff!important;color: white;font-weight: bold;cursor:pointer;font-size:12px;" onclick="kerekapdata()">
                REKAP DATA
              </div>
            </div>
          </div>
          <?php } ?>
          <div class="row">
            <?php foreach ($unitall as $key => $value) {
                echo '<div class="font-icon-list col-xs-6 col-sm-4 col-md-3 menu menuperusahaan">
                  <div class="font-icon-detail" style="min-height: 120px;padding: 30px 12px;background-color: #9368E9!important;color: white;font-weight: bold;cursor:pointer;font-size:12px;" onclick="pilihperkiraan('.$value['doc_id'].',\''.$value['unit'].'\')">
                    '.$value['unit'].'
                  </div>
                </div>';
            }
            ?>
          </div>
        </div>
        <div class="card-footer ">
        </div>
    </div>
</div>
<div class="modal fade modal-primary" id="pilihperkiraan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" style="-webkit-transform: translate(0, 0);-o-transform: translate(0, 0);transform: translate(0, 0);">
        <div class="modal-content">
          <form action="<?=base_url()?>data/<?=$table?>/simpan" method="post">
            <div class="modal-header justify-content-center">
                Menu
            </div>
            <div class="modal-body">
              <input type="hidden" name="aksi" value="ubahnilai">
              <input type="hidden" name="id_perkiraan" class="id_perkiraan" value="">
              <div class="alert alert-success" onclick="kelaporan()" style="cursor:pointer">
                    <span><b> Laporan </b></span>
                </div>
                <?php foreach ($kelompokall as $key => $value) {
                  if ($value['kelompok']!='Bank' && $value['kelompok']!='Pihak 3'){
                    $link=str_replace(' ','_',strtolower($value['kelompok']));
                    echo '<div class="alert alert-primary" onclick="kehalaman(\''.$link.'\')" style="cursor:pointer">
                          <span><b> '.str_replace(' dan ',' & ',$value['kelompok']).' </b></span>
                      </div>';
                  }
                } ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link btn-simple" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
    </div>
</div>

<script type="text/javascript">
var idunitdipilih = 0;
function pilihperkiraan(id,unitusaha){
  idunitdipilih=id;
  $('#pilihperkiraan').modal();
  $('#pilihperkiraan .modal-header').html(unitusaha);
}
function kehalaman(perkiraan='') {
  location.href='<?=base_url()?>data/'+perkiraan+'?unit='+idunitdipilih;
}
function kelaporan() {
  location.href='<?=base_url()?>laporan?unit='+idunitdipilih;
}
function kerekapdata() {
  location.href='<?=base_url()?>rekap';
}
</script>
