<?php
$tahun = isset($_GET['tahun'])?$_GET['tahun']:date('Y');
$unit = isset($_GET['unit'])?$_GET['unit']:1;

$datatotal = $this->sp_model->exec('rekaptotal',['tahun'=>$tahun])['rows'];
$datakredit = $this->sp_model->exec('kredit',['tahun'=>$tahun])['rows'];
$datapemhutang = $this->sp_model->exec('hp3',['tahun'=>$tahun])['rows'];
?>
<div class="col-xs-12">
    <div class="card ">
        <div class="card-header ">
          <form action="" method="get">
            <h4 class="card-title">Tahun <input type="number" class="form-control" name="tahun" onchange="this.form.submit()" value="<?=$tahun?>" style="width: 200px;display: inline-block;"></h4>
          </form>
        </div>
        <div class="card-body ">
          <div id="chart1">
            <canvas id="canvas1"></canvas>
            <hr>
            <div class="stats">
                <i class="fa fa-check"></i> Data Dalam Juta
            </div>
        </div>
        <div id="chart2">
          <canvas id="canvas2"></canvas>
          <hr>
          <div class="stats">
              <i class="fa fa-check"></i> Data Dalam Juta
          </div>
      </div>
        </div>
        <div class="card-footer ">
        </div>
    </div>
</div>

<script type="text/javascript">
window.addEventListener('DOMContentLoaded', (event) => {
  var MONTHS = <?=json_encode(BULAN_FULL)?>;
  var color = Chart.helpers.color;
  var barChartData = {
    labels: [<?php foreach (BULAN as $key => $value) {
      echo "'".$value."',";
    }?>],
    datasets: [{
      label: 'Laba Rugi',
      backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
      borderColor: window.chartColors.red,
      borderWidth: 1,
      data: [<?php foreach ($datatotal as $key => $value) {
          foreach ($datatotal[$key] as $key2 => $value2) {
            if ($key2!='doc_id') {
              if ($key2!='ket') echo number_format(($value2/1000000),2,'.','').',';
            }
          }
      } ?>]
    }, {
      label: 'Pencairan',
      backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
      borderColor: window.chartColors.blue,
      borderWidth: 1,
      data: [<?php foreach ($datakredit as $key => $value) {
          foreach ($datakredit[$key] as $key2 => $value2) {
            if ($key2!='doc_id') {
              if ($key2!='kelompok') echo number_format(($value2/1000000),2,'.','').',';
            }
          }
      } ?>]
    }]

  };

  var barChartData2 = {
    labels: [<?php foreach (BULAN as $key => $value) {
      echo "'".$value."',";
    }?>],
    datasets: [{
      label: 'Pembayaran',
      backgroundColor: color(window.chartColors.green).alpha(0.5).rgbString(),
      borderColor: window.chartColors.green,
      borderWidth: 1,
      data: [<?php foreach ($datapemhutang as $key => $value) {
          foreach ($datapemhutang[$key] as $key2 => $value2) {
            if ($key2!='doc_id') {
              if ($key2!='kelompok') echo number_format((abs($value2)/1000000),2,'.','').',';
            }
          }
      } ?>]
    }]

  };
    var ctx = document.getElementById('canvas1').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: 'Grafik Rekap Data'
					}
				}
			});

    var ctx2 = document.getElementById('canvas2').getContext('2d');
      window.myBar = new Chart(ctx2, {
        type: 'bar',
        data: barChartData2,
        options: {
          responsive: true,
          legend: {
            position: 'top',
          },
          title: {
            display: true,
            text: 'Grafik Pembayaran Bank'
          }
        }
      });

});
</script>
