<?php $pesansession = $this->session->flashdata();?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>template/creative-team/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?=base_url()?>template/creative-team/assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Proyeksi Apps Login</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="<?=base_url()?>assets/css/fonts/montserrat.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/fonts/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="<?=base_url()?>template/creative-team/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=base_url()?>template/creative-team/assets/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?=base_url()?>template/creative-team/assets/css/demo.css" rel="stylesheet" />
</head>

<body>
    <div class="wrapper">
      <div class="main-panel" style="width: 100%!important;">
        <div class="content">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-md-6 ml-auto mr-auto">
                      <div class="card">
                          <div class="header text-center">
                              <h4 class="title">Login</h4>
                              <br>
                          </div>
                          <form action="auth/login" method="post" class="container-fluid" style="padding:30px 20px">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" id="username" name="username" class="form-control" placeholder="Masukkan Username">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" id="password" name="password" class="form-control" placeholder="********">
                            </div>
                            <button type="submit" target="_blank" class="btn btn-round btn-fill btn-info">Login</button>
                          </form>
                      </div>
                  </div>
              </div>

          </div>
        </div>
      <footer class="footer">
          <div class="container-fluid">
              <nav>
                  <ul class="footer-menu">
                      <li>
                          <a href="#">
                              Home
                          </a>
                      </li>
                      <li>
                          <a href="#">
                              Company
                          </a>
                      </li>
                      <li>
                          <a href="#">
                              Portfolio
                          </a>
                      </li>
                      <li>
                          <a href="#">
                              Blog
                          </a>
                      </li>
                  </ul>
                  <p class="copyright text-center">
                      ©
                      <script>
                          document.write(new Date().getFullYear())
                      </script>2019
                      <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                  </p>
              </nav>
          </div>
      </footer>
    </div>
</div>
</body>
<!--   Core JS Files   -->
<script src="<?=base_url()?>template/creative-team/assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>template/creative-team/assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>template/creative-team/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Notifications Plugin    -->
<script src="<?=base_url()?>template/creative-team/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Light Bootstrap Dashboard: scripts for the example pages etc -->
<script src="<?=base_url()?>template/creative-team/assets/js/light-bootstrap-dashboard.js?v=2.0.0 " type="text/javascript"></script>
<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="<?=base_url()?>template/creative-team/assets/js/demo.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  // Javascript method's body can be found in assets/js/demos.js
  <?php if (isset($pesansession['errLoginMsg'])) {
    echo 'demo.showNotification("'.$pesansession['errLoginMsg'].'")';
  } ?>

});
</script>

</html>
