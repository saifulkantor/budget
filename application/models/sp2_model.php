<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sp2_model extends MY_Model{


  function __construct() {
        parent::__construct();
	}
	public $sp = [
    'data_simpan' => 'exec realisasi_simpan @id = [[id_perkiraan]], @tahun = [[tahun]], @Jan = [[Jan]], @Feb = [[Feb]], @Mar = [[Mar]], @Apr = [[Apr]], @Mei = [[Mei]], @Jun = [[Jun]], @Jul = [[Jul]], @Agu =  [[Agu]], @Sep =  [[Sep]], @Okt = [[Okt]], @Nov =  [[Nov]], @Des =  [[Des]];',

    'laporan' => 'exec laporan_realisasi @tahun = [[tahun]] , @unit = [[unit]];',

		'revenue' => 'exec realisasi_all @kode_perkiraan=1, @tanggalmulai = \'[[tanggalmulai]]\' , @tanggalakhir = \'[[tanggalakhir]]\' , @unit = [[unit]];',
		'fixed_cost' => 'exec realisasi_all @kode_perkiraan=2, @tanggalmulai = \'[[tanggalmulai]]\' , @tanggalakhir = \'[[tanggalakhir]]\' , @unit = [[unit]];',
		'variable_cost' => 'exec realisasi_all @kode_perkiraan=3, @tanggalmulai = \'[[tanggalmulai]]\' , @tanggalakhir = \'[[tanggalakhir]]\' , @unit = [[unit]];',
		'fixed_cost_tahunan' => 'exec realisasi_all @kode_perkiraan=4, @tanggalmulai = \'[[tanggalmulai]]\' , @tanggalakhir = \'[[tanggalakhir]]\' , @unit = [[unit]];',
    'bank_dan_kendaraan' => 'exec realisasi_all @kode_perkiraan=5, @tanggalmulai = \'[[tanggalmulai]]\' , @tanggalakhir = \'[[tanggalakhir]]\' , @unit = [[unit]];',
    'capex' => 'exec realisasi_all @kode_perkiraan=6, @tanggalmulai = \'[[tanggalmulai]]\' , @tanggalakhir = \'[[tanggalakhir]]\' , @unit = [[unit]];',
    'csr' => 'exec realisasi_all @kode_perkiraan=7, @tanggalmulai = \'[[tanggalmulai]]\' , @tanggalakhir = \'[[tanggalakhir]]\' , @unit = [[unit]];',
    'project' => 'exec realisasi_all @kode_perkiraan=1009, @tanggalmulai = \'[[tanggalmulai]]\' , @tanggalakhir = \'[[tanggalakhir]]\' , @unit = [[unit]];',

    'pencairan' => 'exec pencairan_realisasi @tahun = [[tahun]]',
    'pembayaran' => 'exec pembayaran_realisasi @tahun = [[tahun]]',
    'ajaxbank' => 'select * from realisasi_bank2 where MONTH(tgl)=[[bulan]] AND  YEAR(tgl)=[[tahun]] AND id_bank=[[id_bank]] AND id_unit=[[unit]]',
    'bank_simpan' => 'exec cu_bank2_realisasi @doc_id=[[doc_id]], @id_bank =[[id_bank]], @id_unit =[[id_unit]], @tgl = \'[[tgl]]\', @amount = [[amount]], @remark =\'[[remark]]\'',

		'rekap' => 'exec sum_utang_realisasi @tahun = [[tahun]];',
		'rekaptotal' => 'exec total @tahun = [[tahun]];',
    'kredit' => 'exec kredit @tahun = [[tahun]];',
    'hp3' => 'exec hp3 @tahun = [[tahun]];',

    'data_simpanket' => 'exec cu_perkiraan @id = [[id_perkiraan]] , @id_unit = [[unit]], @nama = \'[[nama_perkiraan]]\', @id_kelompok=[[id_kelompok]];',
    'data_status' => 'update m_perkiraan set status=[[status]] where id_perkiraan = [[id_perkiraan]];',

    'user' => 'exec cu_user @id_user=\'[[id_user]]\',@username=\'[[username]]\', @password=\'[[password]]\', @nama=\'[[nama]]\', @email=\'[[email]]\', @hakakses=\'[[hakakses]]\', @aktif=[[aktif]], @auth_key=\'[[auth_key]]\';',
    'user_unit' => 'exec cu_user_unit @id_user=\'[[id_user]]\',@id_unit=\'[[id_unit]]\';',


    // Check
    // 'checklock' => 'select case when sum(locked) > 0 then 1 else 0 end [locked] from proyeksi p left outer join m_perkiraan mp on p.id_perkiraan=mp.id_perkiraan where p.id_perkiraan = [[unit]] and p.tahun = [[tahun]] and mp.kode_perkiraan=[[id_kelompok]]',
    'checklock' => 'select case when sum(locked) > 0 then 1 else 0 end [locked] from proyeksi with(nolock) where id_perkiraan = [[unit]] and tahun = [[tahun]]',
    'lockdata' => 'update proyeksi set locked = [[lock]] where id_perkiraan = [[unit]] and tahun = [[tahun]]',
	];

	public function exec($nama_sp, $filters=[]){
		$sql = $this->sp[$nama_sp];
		foreach ($filters as $key => $filter) {
			$sql = str_replace('[['.$key.']]',$filter,$sql);
		}
		$query = $this->db->query($sql);
    if (gettype($query)!='boolean') {
      $json['rows'] = $query->result();
    } else {
      $json['rows']='';
    }

		return $json;
	}
}
