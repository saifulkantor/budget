<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends MY_Model{
	private $table = 'm_user';
	private $id = 'id_user';
	private $kode = 'user_id';
	private $table_detail = '';

    function __construct()
	{
        parent::__construct();
	}


	// digunakan ketika buat master
	public function get($id){
		$sql = "select a.*
				from {$this->table} a
				where a.{$this->id} = ".$id;
		$query = $this->db->query($sql);
		return $query->row();
	}

	public function getAllnoadmin($aktif=1){
		$sql = "select a.*
				from {$this->table} a
				where a.hakakses!='admin' AND aktif=".$aktif;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getAll($aktif=1){
		$sql = "select a.*,b.id_unit
				from {$this->table} a left outer join
				m_user_unit b on a.{$this->id}=b.id_user
				where a.aktif=".$aktif;
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	// digunakan ketika login
	public function getbyusername($username=''){
		$sql = "select a.*,b.id_unit
				from {$this->table} a left outer join
				m_user_unit b on a.{$this->id}=b.id_user
				where a.username = ?";
		$query = $this->db->query($sql, $username);
		return $query->row();
	}

	public function update($dataSet, $dataWhere) {
		$set = implode(', ', array_map(function ($v, $k) {return $k."='".$v."'";}, $dataSet, array_keys($dataSet) ));
		$where = implode(', ', array_map(function ($v, $k) {return $k."='".$v."'";}, $dataWhere, array_keys($dataWhere) ));
		$sql = "update {$this->table} SET {$set}
				where {$where}";
		$query = $this->db->query($sql);
		return $query;
	}

}
