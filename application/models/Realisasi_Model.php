<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Realisasi_Model extends MY_Model{
	private $table = 'realisasi';
	private $id = 'id_perkiraan';
	private $table_detail = '';

    function __construct()
	{
        parent::__construct();
	}

	public function grid($paging, $filter){
		if (!isset($paging['sort'])) {
			$paging['sort'] = $this->id;
		}

		$json = array('total' => 0, 'rows' => array());

		$tempSql = 'id_user, username, aktif, auth_key';

		$filter['sql'] = generateFilter($filter['sql'], $tempSql);

		// dapatkan total terlebih dahulu
		$sql = "select count({$this->id}) as total
				from {$this->table}
				where 1=1 {$filter['sql']}";
		$query = $this->db->query($sql, $filter['param']);
		$json['total'] = $query->row()->total;

		// replace dengan kolom yg ditentukan
		$sql = str_replace("count({$this->id}) as total", $tempSql, $sql);

		// tambahkan paging
		$sql .= " order by {$paging['sort']} {$paging['order']}
				  offset {$paging['offset']} rows fetch next {$paging['rows']} rows only";
		$query = $this->db->query($sql, $filter['param']);
		$json['rows'] = $query->result();

		return $json;
	}

	// digunakan ketika buat master
	public function get($id='',$tanggal=''){
		$sql = "select a.*
				from {$this->table} a
				where a.{$this->id} = $id and a.tanggal = '$tanggal'";
		$query = $this->db->query($sql);
		if ($query) {
			$msg = generateMessage(true);
			$msg['data'] = $query->row();
			return $msg;
		} else {
			$err = $this->db->error();
			return generateMessage(false, $err['message'], 'Peringatan', 'error');
		}
	}

	public function getAll(){
		$sql = "select a.*
				from {$this->table} a";
		$query = $this->db->query($sql);
		if ($query) {
			$msg = generateMessage(true);
			$msg['data'] = $query->result_array();
			return $msg;
		} else {
			$err = $this->db->error();
			return generateMessage(false, $err['message'], 'Peringatan', 'error');
		}
	}

	public function getArsip($id_kelompok=0,$id_perusahaan=1){
		$sql = "select a.*
				from {$this->table} a WHERE a.status=0 AND a.kode_perkiraan=".$id_kelompok." AND id_perusahaan=".$id_perusahaan;
		$query = $this->db->query($sql);
		if ($query) {
			$msg = generateMessage(true);
			$msg['data'] = $query->result_array();
			return $msg;
		} else {
			$err = $this->db->error();
			return generateMessage(false, $err['message'], 'Peringatan', 'error');
		}
	}


	// digunakan ketika login
	public function getbyusername($username=''){
		$sql = "select a.id_user,  a.username, a.password
				from {$this->table} a
				where a.username = ?";
		$query = $this->db->query($sql, $username);
		return $query->row();
	}

	// combogrid
	public function cgrid($q){
		$tempSql = '';
		$sql = "select id_user, username
				from {$this->table}
				where aktif = 1 and
					  username like ?
					  {$tempSql}";
		$query = $this->db->query($sql, array('%'.$q.'%'));

		return $query->result();
	}

	public function getDuplicate($kode){
		$sql = "select {$this->id} as id
				from {$this->table}
				where {$this->kode} = ?";
		$query = $this->db->query($sql, $kode);
		$row = $query->row();
		return $row->id ?? null;
	}

	public function save($data, $detailAkses, $detailPerusahaan, $detailGudang) {
		// start trans
		$this->db->trans_begin();

		$sql = generateSqlInsert($this->table, $data);
		$query = $this->db->query($sql['sql'], $sql['param']);

		// get last id
		$query = $this->db->query('select @@IDENTITY as last_id');
		$lastID = $query->row()->last_id;

		// hash id
		$dataUpdate = array('auth_key' => password_hash($lastID, PASSWORD_BCRYPT));

		// update data
		$sql = generateSqlUpdate($this->table, $dataUpdate, array($this->id=>$lastID));
		$query = $this->db->query($sql['sql'], $sql['param']);

		// insert akses,
		if (count($detailAkses) > 0) {
			$msg = $this->insertDetailAkses($lastID, $detailAkses);
			if (!$msg['success']) {
				// rollback
				$this->db->trans_rollback();

				return $msg;
			}
		}

		// insert perusahaan
		if (count($detailPerusahaan) > 0) {
			$msg = $this->insertDetailPerusahaan($lastID, $detailPerusahaan);
			if (!$msg['success']) {
				// rollback
				$this->db->trans_rollback();

				return $msg;
			}
		}

		// insert gudang
		if (count($detailGudang) > 0) {
			$msg = $this->insertDetailGudang($lastID, $detailGudang);
			if (!$msg['success']) {
				// rollback
				$this->db->trans_rollback();

				return $msg;
			}
		}

		if ($this->db->trans_status() === FALSE) {
			// rollback
			$this->db->trans_rollback();

			$err = $this->db->error();
			return generateMessage(false, $err['message'], 'Peringatan', 'error');
		} else {
			// commit
			$this->db->trans_commit();

			return generateMessage(true);
		}
	}

	public function update($dataSet, $dataWhere, $detailAkses, $detailPerusahaan, $detailGudang) {
		// start trans
		$this->db->trans_begin();

		$sql = generateSqlUpdate($this->table, $dataSet, $dataWhere);

		$this->db->query($sql['sql'], $sql['param']);

		// insert akses,
		if (count($detailAkses) > 0) {
			$msg = $this->insertDetailAkses($dataWhere[$this->id], $detailAkses);
			if (!$msg['success']) {
				// rollback
				$this->db->trans_rollback();

				return $msg;
			}
		}

		// insert perusahaan
		if (count($detailPerusahaan) > 0) {
			$msg = $this->insertDetailPerusahaan($dataWhere[$this->id], $detailPerusahaan);
			if (!$msg['success']) {
				// rollback
				$this->db->trans_rollback();

				return $msg;
			}
		}

		// insert gudang
		if (count($detailGudang) > 0) {
			$msg = $this->insertDetailGudang($dataWhere[$this->id], $detailGudang);
			if (!$msg['success']) {
				// rollback
				$this->db->trans_rollback();

				return $msg;
			}
		}

		if ($this->db->trans_status() === FALSE) {
			// rollback
			$this->db->trans_rollback();

			$err = $this->db->error();
			return generateMessage(false, $err['message'], 'Peringatan', 'error');
		} else {
			// commit
			$this->db->trans_commit();

			return generateMessage(true);
		}
	}

	function insertDetailAkses($id, $detail){
		$this->table_detail = 'm_user_akses';

		// create temporary table
		$this->db->query('SELECT * INTO #'.$this->table_detail.' FROM '.$this->table_detail.' WHERE 1=0');

		// drop some field
		$this->db->query('ALTER TABLE #'.$this->table_detail.' DROP COLUMN '.$this->id.'_dtl');

		foreach($detail as $item) {
			$detailInsert = array(
				'id_user' => $id,
				'id_menu' => $item->id,
				'akses' => empty($item->akses) ? 0 : 1,
				'tambah' => empty($item->tambah) ? 0 : 1,
				'ubah' => empty($item->ubah) ? 0 : 1,
				'hapus' => empty($item->hapus) ? 0 : 1,
			);

			$sql = generateSqlInsert('#'.$this->table_detail, $detailInsert);
			//print_r($sql['param']);
			$query = $this->db->query($sql['sql'], $sql['param']);
		}

		// insert all data to real table from temp table
		$query = $this->db->query('INSERT INTO '.$this->table_detail.' SELECT * FROM #'.$this->table_detail);

		return generateMessage(true);
	}

	function insertDetailPerusahaan($id, $detail){
		$this->table_detail = 'm_user_perusahaan';

		// create temporary table
		$this->db->query('SELECT * INTO #'.$this->table_detail.' FROM '.$this->table_detail.' WHERE 1=0');

		// drop some field
		$this->db->query('ALTER TABLE #'.$this->table_detail.' DROP COLUMN '.$this->id.'_dtl');

		foreach($detail as $item) {
			if ($item->cek == 1) {
				$detailInsert = array(
					'id_user' => $id,
					'id_perusahaan' => $item->id_perusahaan,
				);

				$sql = generateSqlInsert('#'.$this->table_detail, $detailInsert);
				$query = $this->db->query($sql['sql'], $sql['param']);
			}
		}

		// insert all data to real table from temp table
		$query = $this->db->query('INSERT INTO '.$this->table_detail.' SELECT * FROM #'.$this->table_detail);

		return generateMessage(true);
	}

	function insertDetailGudang($id, $detail){
		$this->table_detail = 'm_user_gudang';

		// create temporary table
		$this->db->query('SELECT * INTO #'.$this->table_detail.' FROM '.$this->table_detail.' WHERE 1=0');

		// drop some field
		$this->db->query('ALTER TABLE #'.$this->table_detail.' DROP COLUMN '.$this->id.'_dtl');

		foreach($detail as $item) {
			if ($item->cek == 1) {
				$detailInsert = array(
					'id_user' => $id,
					'id_gudang' => $item->id_gudang,
				);

				$sql = generateSqlInsert('#'.$this->table_detail, $detailInsert);
				$query = $this->db->query($sql['sql'], $sql['param']);
			}
		}

		// insert all data to real table from temp table
		$query = $this->db->query('INSERT INTO '.$this->table_detail.' SELECT * FROM #'.$this->table_detail);

		return generateMessage(true);
	}

	public function delete($id){
		$sql = "delete from {$this->table} where {$this->id} = ?";
		$query = $this->db->query($sql, $id);

		// delete detail
		$this->db->query("delete from m_user_akses where {$this->id} = ?", $id);
		$this->db->query("delete from m_user_perusahaan where {$this->id} = ?", $id);
		$this->db->query("delete from m_user_gudang where {$this->id} = ?", $id);

		if ($query) {
			return generateMessage(true);
		} else {
			$err = $this->db->error();
			return generateMessage(false, $err['message'], 'Error', 'error');
		}
	}

	public function getAkses($id) {
		$sql = "select a.*, c.akses, c.tambah, c.ubah, c.hapus
				from m_menu a
				inner join m_menu_header b on a.modul = b.nama
				left join m_user_akses c on a.id = c.id_menu and c.id_user = {$id}
				order by b.urutan, a.urutan";
		$query = $this->db->query($sql);

		return $query->result();
	}

	public function getPerusahaan($id) {
		$sql = "select a.id_perusahaan, a.kode_perusahaan, a.nama_perusahaan, a.auth_key, iif(b.id_user is null, 0, 1) as cek
				from m_perusahaan a
				left join m_user_perusahaan b on a.id_perusahaan = b.id_perusahaan and b.id_user = {$id}
				where a.aktif = 1";
		$query = $this->db->query($sql);

		return $query->result();
	}

	public function getGudang($id) {
		$sql = "select id_gudang, kode_gudang, nama_gudang, kode_perusahaan, sum(cek) as cek
				from
				(
					select b.id_gudang, b.kode_gudang, b.nama_gudang, c.kode_perusahaan, 0 as cek
					from m_user_perusahaan a
					inner join m_gudang b on a.id_perusahaan = b.id_perusahaan
					inner join m_perusahaan c on a.id_perusahaan = c.id_perusahaan
					where b.aktif = 1 and a.id_user = {$id}

					union

					select b.id_gudang, b.kode_gudang, b.nama_gudang, c.kode_perusahaan, 1 as cek
					from m_user_gudang a
					inner join m_gudang b on a.id_gudang = b.id_gudang
					inner join m_perusahaan c on b.id_perusahaan = c.id_perusahaan
					where b.aktif = 1 and a.id_user = {$id} ) as t
				group by id_gudang, kode_gudang, nama_gudang, kode_perusahaan";
		$query = $this->db->query($sql);

		return $query->result();
	}
}
